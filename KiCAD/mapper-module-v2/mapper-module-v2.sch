EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "SEGA Mapper Module"
Date "2022-01-27"
Rev "2.2"
Comp "heavydeck.net"
Comment1 "See COPYING or https://creativecommons.org/licenses/by-sa/4.0 for the license text"
Comment2 "Released under the Creative Commons Attribution Share-Alike license"
Comment3 "Copyright J.Luis Álvarez 2022"
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS573 U1
U 1 1 5D149D50
P 6850 2050
F 0 "U1" H 6850 3031 50  0000 C CNN
F 1 "74HC573" H 6850 2940 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 6850 2050 50  0001 C CNN
F 3 "74xx/74hc573.pdf" H 6850 2050 50  0001 C CNN
	1    6850 2050
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS573 U2
U 1 1 5D14AB5D
P 6850 5050
F 0 "U2" H 6850 6031 50  0000 C CNN
F 1 "74HC573" H 6850 5940 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 6850 5050 50  0001 C CNN
F 3 "74xx/74hc573.pdf" H 6850 5050 50  0001 C CNN
	1    6850 5050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J1
U 1 1 5D14EA51
P 10150 1500
F 0 "J1" V 10154 413 50  0000 R CNN
F 1 "Conn_02x20_Odd_Even" V 10245 413 50  0001 R CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x20_Pitch2.54mm" H 10150 1500 50  0001 C CNN
F 3 "~" H 10150 1500 50  0001 C CNN
	1    10150 1500
	0    1    -1   0   
$EndComp
$Comp
L pspice:CAP C1
U 1 1 5D152BBD
P 8950 4400
F 0 "C1" H 9128 4446 50  0000 L CNN
F 1 "100nF" H 9128 4355 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 8950 4400 50  0001 C CNN
F 3 "~" H 8950 4400 50  0001 C CNN
	1    8950 4400
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C2
U 1 1 5D153955
P 9550 4400
F 0 "C2" H 9728 4446 50  0000 L CNN
F 1 "100nF" H 9728 4355 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 9550 4400 50  0001 C CNN
F 3 "~" H 9550 4400 50  0001 C CNN
	1    9550 4400
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C3
U 1 1 5D153B9A
P 10150 4400
F 0 "C3" H 10328 4446 50  0000 L CNN
F 1 "100nF" H 10328 4355 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 10150 4400 50  0001 C CNN
F 3 "~" H 10150 4400 50  0001 C CNN
	1    10150 4400
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C4
U 1 1 5D153E8F
P 10750 4400
F 0 "C4" H 10928 4446 50  0000 L CNN
F 1 "100nF" H 10928 4355 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 10750 4400 50  0001 C CNN
F 3 "~" H 10750 4400 50  0001 C CNN
	1    10750 4400
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0101
U 1 1 5D14BE5E
P 9950 950
F 0 "#PWR0101" H 9950 800 50  0001 C CNN
F 1 "VCC" H 9967 1123 50  0000 C CNN
F 2 "" H 9950 950 50  0001 C CNN
F 3 "" H 9950 950 50  0001 C CNN
	1    9950 950 
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 5D14C7F8
P 11050 1900
F 0 "#PWR0102" H 11050 1750 50  0001 C CNN
F 1 "VCC" H 11068 2073 50  0000 C CNN
F 2 "" H 11050 1900 50  0001 C CNN
F 3 "" H 11050 1900 50  0001 C CNN
	1    11050 1900
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5D14CBAA
P 9150 1800
F 0 "#PWR0103" H 9150 1550 50  0001 C CNN
F 1 "GND" H 9155 1627 50  0000 C CNN
F 2 "" H 9150 1800 50  0001 C CNN
F 3 "" H 9150 1800 50  0001 C CNN
	1    9150 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 1800 9150 1750
Wire Wire Line
	9150 1750 9250 1750
Wire Wire Line
	9450 1750 9450 1700
Connection ~ 9150 1750
Wire Wire Line
	9150 1750 9150 1700
Wire Wire Line
	9350 1700 9350 1750
Connection ~ 9350 1750
Wire Wire Line
	9350 1750 9450 1750
Wire Wire Line
	9250 1700 9250 1750
Connection ~ 9250 1750
Wire Wire Line
	9250 1750 9350 1750
Text Label 9150 1100 1    50   ~ 0
ROM-A18
Text Label 9250 1100 1    50   ~ 0
ROM-A17
Text Label 9350 1100 1    50   ~ 0
ROM-A16
Text Label 9450 1100 1    50   ~ 0
ROM-A15
Text Label 9550 1100 1    50   ~ 0
ROM-A14
Text Label 9550 1800 3    50   ~ 0
A14
Text Label 9650 1100 1    50   ~ 0
~WR
Text Label 9750 1100 1    50   ~ 0
~MREQ
Text Label 9850 1100 1    50   ~ 0
D3
Text Label 10050 1100 1    50   ~ 0
A13
Text Label 10150 1100 1    50   ~ 0
A8
Text Label 10250 1100 1    50   ~ 0
A9
Text Label 10350 1100 1    50   ~ 0
A11
Text Label 10450 1100 1    50   ~ 0
~RD
Text Label 10550 1100 1    50   ~ 0
A10
Text Label 10650 1100 1    50   ~ 0
~ROM-CE
Text Label 11050 1100 1    50   ~ 0
D4
Text Label 9750 1800 3    50   ~ 0
~CE
Text Label 9850 1800 3    50   ~ 0
A12
Text Label 9950 1800 3    50   ~ 0
A7
Text Label 10050 1800 3    50   ~ 0
A6
Text Label 10150 1800 3    50   ~ 0
A5
Text Label 10250 1800 3    50   ~ 0
A4
Text Label 10350 1800 3    50   ~ 0
A3
Text Label 10450 1800 3    50   ~ 0
A2
Text Label 10550 1800 3    50   ~ 0
A1
Text Label 10650 1800 3    50   ~ 0
A0
Text Label 10750 1800 3    50   ~ 0
D0
Text Label 10850 1800 3    50   ~ 0
D1
Text Label 10950 1800 3    50   ~ 0
D2
Wire Wire Line
	9550 700  9550 1200
Wire Wire Line
	9650 700  9650 1200
Wire Wire Line
	9750 700  9750 1200
Wire Wire Line
	9850 700  9850 1200
Wire Wire Line
	10050 700  10050 1200
Wire Wire Line
	10150 700  10150 1200
Wire Wire Line
	10250 1200 10250 700 
Wire Wire Line
	10350 700  10350 1200
Wire Wire Line
	10450 1200 10450 700 
Wire Wire Line
	10550 700  10550 1200
Wire Wire Line
	10650 1200 10650 700 
Wire Wire Line
	11050 1200 11050 700 
Wire Wire Line
	9550 1700 9550 2150
Wire Wire Line
	9650 2150 9650 1700
Wire Wire Line
	9750 1700 9750 2150
Wire Wire Line
	9850 2150 9850 1700
Wire Wire Line
	9950 1700 9950 2150
Wire Wire Line
	10050 2150 10050 1700
Wire Wire Line
	10150 1700 10150 2150
Wire Wire Line
	10250 2150 10250 1700
Wire Wire Line
	10350 1700 10350 2150
Wire Wire Line
	10450 2150 10450 1700
Wire Wire Line
	10550 1700 10550 2150
Wire Wire Line
	10650 2150 10650 1700
Wire Wire Line
	10750 1700 10750 2150
Wire Wire Line
	10850 2150 10850 1700
Wire Wire Line
	10950 1700 10950 2150
$Comp
L power:GND #PWR0104
U 1 1 5D165229
P 8950 4700
F 0 "#PWR0104" H 8950 4450 50  0001 C CNN
F 1 "GND" H 8955 4527 50  0000 C CNN
F 2 "" H 8950 4700 50  0001 C CNN
F 3 "" H 8950 4700 50  0001 C CNN
	1    8950 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5D1658AC
P 9550 4700
F 0 "#PWR0105" H 9550 4450 50  0001 C CNN
F 1 "GND" H 9555 4527 50  0000 C CNN
F 2 "" H 9550 4700 50  0001 C CNN
F 3 "" H 9550 4700 50  0001 C CNN
	1    9550 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5D165DB5
P 10150 4700
F 0 "#PWR0106" H 10150 4450 50  0001 C CNN
F 1 "GND" H 10155 4527 50  0000 C CNN
F 2 "" H 10150 4700 50  0001 C CNN
F 3 "" H 10150 4700 50  0001 C CNN
	1    10150 4700
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0108
U 1 1 5D166FB6
P 8950 4100
F 0 "#PWR0108" H 8950 3950 50  0001 C CNN
F 1 "VCC" H 8967 4273 50  0000 C CNN
F 2 "" H 8950 4100 50  0001 C CNN
F 3 "" H 8950 4100 50  0001 C CNN
	1    8950 4100
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0109
U 1 1 5D1674FE
P 9550 4100
F 0 "#PWR0109" H 9550 3950 50  0001 C CNN
F 1 "VCC" H 9567 4273 50  0000 C CNN
F 2 "" H 9550 4100 50  0001 C CNN
F 3 "" H 9550 4100 50  0001 C CNN
	1    9550 4100
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0110
U 1 1 5D167866
P 10150 4100
F 0 "#PWR0110" H 10150 3950 50  0001 C CNN
F 1 "VCC" H 10167 4273 50  0000 C CNN
F 2 "" H 10150 4100 50  0001 C CNN
F 3 "" H 10150 4100 50  0001 C CNN
	1    10150 4100
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0114
U 1 1 5D170B85
P 6850 950
F 0 "#PWR0114" H 6850 800 50  0001 C CNN
F 1 "VCC" H 6867 1123 50  0000 C CNN
F 2 "" H 6850 950 50  0001 C CNN
F 3 "" H 6850 950 50  0001 C CNN
	1    6850 950 
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0115
U 1 1 5D170B8F
P 6850 3950
F 0 "#PWR0115" H 6850 3800 50  0001 C CNN
F 1 "VCC" H 6867 4123 50  0000 C CNN
F 2 "" H 6850 3950 50  0001 C CNN
F 3 "" H 6850 3950 50  0001 C CNN
	1    6850 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 950  6850 1250
Wire Wire Line
	6850 3950 6850 4250
$Comp
L power:GND #PWR0118
U 1 1 5D18CB8D
P 6850 2950
F 0 "#PWR0118" H 6850 2700 50  0001 C CNN
F 1 "GND" H 6855 2777 50  0000 C CNN
F 2 "" H 6850 2950 50  0001 C CNN
F 3 "" H 6850 2950 50  0001 C CNN
	1    6850 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5D18CB97
P 6850 5950
F 0 "#PWR0119" H 6850 5700 50  0001 C CNN
F 1 "GND" H 6855 5777 50  0000 C CNN
F 2 "" H 6850 5950 50  0001 C CNN
F 3 "" H 6850 5950 50  0001 C CNN
	1    6850 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 5850 6850 5950
Wire Wire Line
	6850 2950 6850 2850
Wire Wire Line
	8200 2300 8200 1750
Wire Wire Line
	7700 2300 7700 1950
$Comp
L power:GND #PWR0120
U 1 1 5D1B3B76
P 7700 3200
F 0 "#PWR0120" H 7700 2950 50  0001 C CNN
F 1 "GND" H 7705 3027 50  0000 C CNN
F 2 "" H 7700 3200 50  0001 C CNN
F 3 "" H 7700 3200 50  0001 C CNN
	1    7700 3200
	1    0    0    -1  
$EndComp
NoConn ~ 10750 1200
NoConn ~ 10850 1200
NoConn ~ 10950 1200
Wire Wire Line
	6350 5250 6200 5250
Wire Wire Line
	6200 5250 6200 5150
Connection ~ 6200 4950
Wire Wire Line
	6200 4950 6350 4950
Wire Wire Line
	6350 5050 6200 5050
Connection ~ 6200 5050
Wire Wire Line
	6200 5050 6200 4950
Wire Wire Line
	6350 5150 6200 5150
Connection ~ 6200 5150
Wire Wire Line
	6200 5150 6200 5050
Text Label 5950 4550 0    50   ~ 0
D0
Text Label 5950 4650 0    50   ~ 0
D1
Text Label 5950 4750 0    50   ~ 0
D2
Text Label 5950 4850 0    50   ~ 0
D3
Text Label 5950 4950 0    50   ~ 0
D4
Wire Wire Line
	6350 2250 6200 2250
Wire Wire Line
	6200 2250 6200 2150
Connection ~ 6200 1950
Wire Wire Line
	6200 1950 6350 1950
Wire Wire Line
	6350 2050 6200 2050
Connection ~ 6200 2050
Wire Wire Line
	6200 2050 6200 1950
Wire Wire Line
	6350 2150 6200 2150
Connection ~ 6200 2150
Wire Wire Line
	6200 2150 6200 2050
Text Label 5950 1550 0    50   ~ 0
D0
Text Label 5950 1650 0    50   ~ 0
D1
Text Label 5950 1750 0    50   ~ 0
D2
Text Label 5950 1850 0    50   ~ 0
D3
Text Label 5950 1950 0    50   ~ 0
D4
Text Label 7400 4550 0    50   ~ 0
ROM-A14
Text Label 7400 4650 0    50   ~ 0
ROM-A15
Text Label 7400 4750 0    50   ~ 0
ROM-A16
Text Label 7400 4850 0    50   ~ 0
ROM-A17
Text Label 7400 4950 0    50   ~ 0
ROM-A18
NoConn ~ 7350 5250
NoConn ~ 7350 5150
NoConn ~ 7350 5050
Text Label 7400 1550 0    50   ~ 0
ROM-A14
Text Label 7400 1650 0    50   ~ 0
ROM-A15
Text Label 7400 1750 0    50   ~ 0
ROM-A16
Text Label 7400 1850 0    50   ~ 0
ROM-A17
Text Label 7400 1950 0    50   ~ 0
ROM-A18
NoConn ~ 7350 2250
NoConn ~ 7350 2150
NoConn ~ 7350 2050
Text Label 4900 5800 0    50   ~ 0
~ROM-CE
Text Label 5900 5550 0    50   ~ 0
~SLOT1-OE
Text Label 5900 2550 0    50   ~ 0
~SLOT2-OE
Text Label 5900 5450 0    50   ~ 0
SLOT1-LD
Text Label 5900 2450 0    50   ~ 0
SLOT2-LD
$Comp
L Device:R R1
U 1 1 5D157921
P 8150 1350
F 0 "R1" H 8220 1396 50  0000 L CNN
F 1 "10K" H 8220 1305 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" V 8080 1350 50  0001 C CNN
F 3 "~" H 8150 1350 50  0001 C CNN
	1    8150 1350
	-1   0    0    1   
$EndComp
Text Label 8500 1150 2    50   ~ 0
A14
Wire Wire Line
	9150 1200 9150 700 
Wire Wire Line
	9250 700  9250 1200
Wire Wire Line
	9350 1200 9350 700 
Wire Wire Line
	9450 700  9450 1200
Text Notes 9650 550  0    50   ~ 0
Pin header connector
Wire Wire Line
	9950 950  9950 1200
Wire Wire Line
	11050 1700 11050 1900
Entry Wire Line
	9050 600  9150 700 
Entry Wire Line
	9150 600  9250 700 
Entry Wire Line
	9250 600  9350 700 
Entry Wire Line
	9350 600  9450 700 
Entry Wire Line
	9450 600  9550 700 
Entry Wire Line
	9550 600  9650 700 
Entry Wire Line
	9650 600  9750 700 
Entry Wire Line
	9750 600  9850 700 
Entry Wire Line
	9950 600  10050 700 
Entry Wire Line
	10050 600  10150 700 
Entry Wire Line
	10150 600  10250 700 
Entry Wire Line
	10250 600  10350 700 
Entry Wire Line
	10350 600  10450 700 
Entry Wire Line
	10450 600  10550 700 
Entry Wire Line
	10550 600  10650 700 
Entry Wire Line
	10950 600  11050 700 
Entry Wire Line
	10950 2150 11050 2250
Entry Wire Line
	10850 2150 10950 2250
Entry Wire Line
	10750 2150 10850 2250
Entry Wire Line
	10650 2150 10750 2250
Entry Wire Line
	10550 2150 10650 2250
Entry Wire Line
	10450 2150 10550 2250
Entry Wire Line
	10350 2150 10450 2250
Entry Wire Line
	10250 2150 10350 2250
Entry Wire Line
	10150 2150 10250 2250
Entry Wire Line
	10050 2150 10150 2250
Entry Wire Line
	9950 2150 10050 2250
Entry Wire Line
	9850 2150 9950 2250
Entry Wire Line
	9750 2150 9850 2250
Entry Wire Line
	9650 2150 9750 2250
Entry Wire Line
	9550 2150 9650 2250
Wire Wire Line
	7350 4550 8600 4550
Wire Wire Line
	7350 4650 8600 4650
Wire Wire Line
	7350 4750 8600 4750
Wire Wire Line
	7350 4850 8600 4850
Wire Wire Line
	7350 4950 8600 4950
Entry Wire Line
	8600 1150 8700 1250
Entry Wire Line
	8600 1950 8700 2050
Entry Wire Line
	8600 1850 8700 1950
Entry Wire Line
	8600 1750 8700 1850
Entry Wire Line
	8600 1650 8700 1750
Entry Wire Line
	8600 1550 8700 1650
Entry Wire Line
	8600 4950 8700 5050
Entry Wire Line
	8600 4850 8700 4950
Entry Wire Line
	8600 4750 8700 4850
Entry Wire Line
	8600 4650 8700 4750
Entry Wire Line
	8600 4550 8700 4650
Wire Bus Line
	8700 6350 5450 6350
Wire Wire Line
	5550 1550 6350 1550
Wire Wire Line
	5550 1650 6350 1650
Wire Wire Line
	5550 1750 6350 1750
Wire Wire Line
	5550 1850 6350 1850
Wire Wire Line
	5550 1950 6200 1950
Wire Wire Line
	5550 4550 6350 4550
Wire Wire Line
	5550 4650 6350 4650
Wire Wire Line
	5550 4750 6350 4750
Wire Wire Line
	5550 4850 6350 4850
Wire Wire Line
	5550 4950 6200 4950
Entry Wire Line
	5450 4850 5550 4950
Entry Wire Line
	5450 4750 5550 4850
Entry Wire Line
	5450 4650 5550 4750
Entry Wire Line
	5450 4550 5550 4650
Entry Wire Line
	5450 4450 5550 4550
Entry Wire Line
	5450 1450 5550 1550
Entry Wire Line
	5450 1550 5550 1650
Entry Wire Line
	5450 1650 5550 1750
Entry Wire Line
	5450 1750 5550 1850
Entry Wire Line
	5450 1850 5550 1950
Entry Wire Line
	5350 5800 5450 5900
Text Notes 6650 700  0    50   ~ 0
SLOT2 Latch
Text Notes 6600 3700 0    50   ~ 0
SLOT1 Latch
Text Notes 7600 3100 1    50   ~ 0
SLOT0\n(Fixed to 0b0000)
Text Notes 8750 3750 0    50   ~ 0
Bypass caps
Text Notes 9700 2450 0    50   ~ 0
"Flip" connector row
$Comp
L Connector_Generic:Conn_01x20 J2
U 1 1 5D1B823A
P 10150 2800
F 0 "J2" V 10367 2746 50  0000 C CNN
F 1 "Conn_01x20" V 10276 2746 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x20_Pitch2.54mm" H 10150 2800 50  0001 C CNN
F 3 "~" H 10150 2800 50  0001 C CNN
	1    10150 2800
	0    1    -1   0   
$EndComp
$Comp
L power:VCC #PWR02
U 1 1 5D1CA3F4
P 11050 3200
F 0 "#PWR02" H 11050 3050 50  0001 C CNN
F 1 "VCC" H 11068 3373 50  0000 C CNN
F 2 "" H 11050 3200 50  0001 C CNN
F 3 "" H 11050 3200 50  0001 C CNN
	1    11050 3200
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5D1CA3FA
P 9150 3100
F 0 "#PWR01" H 9150 2850 50  0001 C CNN
F 1 "GND" H 9155 2927 50  0000 C CNN
F 2 "" H 9150 3100 50  0001 C CNN
F 3 "" H 9150 3100 50  0001 C CNN
	1    9150 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 3100 9150 3050
Wire Wire Line
	9150 3050 9250 3050
Wire Wire Line
	9450 3050 9450 3000
Connection ~ 9150 3050
Wire Wire Line
	9150 3050 9150 3000
Wire Wire Line
	9350 3000 9350 3050
Connection ~ 9350 3050
Wire Wire Line
	9350 3050 9450 3050
Wire Wire Line
	9250 3000 9250 3050
Connection ~ 9250 3050
Wire Wire Line
	9250 3050 9350 3050
Text Label 9550 3100 3    50   ~ 0
A14
Text Label 9650 3100 3    50   ~ 0
~M0-7
Text Label 9750 3100 3    50   ~ 0
~CE
Text Label 9850 3100 3    50   ~ 0
A12
Text Label 9950 3100 3    50   ~ 0
A7
Text Label 10050 3100 3    50   ~ 0
A6
Text Label 10150 3100 3    50   ~ 0
A5
Text Label 10250 3100 3    50   ~ 0
A4
Text Label 10350 3100 3    50   ~ 0
A3
Text Label 10450 3100 3    50   ~ 0
A2
Text Label 10550 3100 3    50   ~ 0
A1
Text Label 10650 3100 3    50   ~ 0
A0
Text Label 10750 3100 3    50   ~ 0
D0
Text Label 10850 3100 3    50   ~ 0
D1
Text Label 10950 3100 3    50   ~ 0
D2
Wire Wire Line
	9550 3000 9550 3450
Wire Wire Line
	9650 3450 9650 3000
Wire Wire Line
	9750 3000 9750 3450
Wire Wire Line
	9850 3450 9850 3000
Wire Wire Line
	9950 3000 9950 3450
Wire Wire Line
	10050 3450 10050 3000
Wire Wire Line
	10150 3000 10150 3450
Wire Wire Line
	10250 3450 10250 3000
Wire Wire Line
	10350 3000 10350 3450
Wire Wire Line
	10450 3450 10450 3000
Wire Wire Line
	10550 3000 10550 3450
Wire Wire Line
	10650 3450 10650 3000
Wire Wire Line
	10750 3000 10750 3450
Wire Wire Line
	10850 3450 10850 3000
Wire Wire Line
	10950 3000 10950 3450
Wire Wire Line
	11050 3000 11050 3200
Entry Wire Line
	10950 3450 11050 3550
Entry Wire Line
	10850 3450 10950 3550
Entry Wire Line
	10750 3450 10850 3550
Entry Wire Line
	10650 3450 10750 3550
Entry Wire Line
	10550 3450 10650 3550
Entry Wire Line
	10450 3450 10550 3550
Entry Wire Line
	10350 3450 10450 3550
Entry Wire Line
	10250 3450 10350 3550
Entry Wire Line
	10150 3450 10250 3550
Entry Wire Line
	10050 3450 10150 3550
Entry Wire Line
	9950 3450 10050 3550
Entry Wire Line
	9850 3450 9950 3550
Entry Wire Line
	9750 3450 9850 3550
Entry Wire Line
	9650 3450 9750 3550
Entry Wire Line
	9550 3450 9650 3550
$Comp
L 74xx:74LS21 U4
U 2 1 61F38F4B
P 2300 1300
F 0 "U4" H 2300 1675 50  0000 C CNN
F 1 "74HC21" H 2300 1584 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 2300 1300 50  0001 C CNN
F 3 "" H 2300 1300 50  0001 C CNN
	2    2300 1300
	0    1    1    0   
$EndComp
$Comp
L 74xx:74LS21 U4
U 1 1 61F3C983
P 1600 1300
F 0 "U4" H 1600 1675 50  0000 C CNN
F 1 "74HC21" H 1600 1584 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 1600 1300 50  0001 C CNN
F 3 "" H 1600 1300 50  0001 C CNN
	1    1600 1300
	0    1    1    0   
$EndComp
$Comp
L 74xx:74LS21 U5
U 2 1 61F3DB58
P 2950 1300
F 0 "U5" H 2950 1675 50  0000 C CNN
F 1 "74HC21" H 2950 1584 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 2950 1300 50  0001 C CNN
F 3 "" H 2950 1300 50  0001 C CNN
	2    2950 1300
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 1000 3100 700 
Text Label 1550 800  3    50   ~ 0
A2
Text Label 1450 800  3    50   ~ 0
A3
Text Label 2350 750  3    50   ~ 0
A4
Text Label 2450 750  3    50   ~ 0
A5
Text Label 2800 800  3    50   ~ 0
A6
Text Label 2900 800  3    50   ~ 0
A7
Text Label 2250 750  3    50   ~ 0
A8
Text Label 2150 750  3    50   ~ 0
A9
Text Label 1750 750  3    50   ~ 0
A10
Text Label 1650 750  3    50   ~ 0
A11
Text Label 3000 750  3    50   ~ 0
A12
Text Label 3100 750  3    50   ~ 0
A13
Wire Wire Line
	3000 700  3000 1000
Wire Wire Line
	2900 1000 2900 700 
Wire Wire Line
	2800 1000 2800 700 
Wire Wire Line
	2450 1000 2450 700 
Wire Wire Line
	2350 700  2350 1000
Wire Wire Line
	2250 1000 2250 700 
Wire Wire Line
	2150 1000 2150 700 
Wire Wire Line
	1750 1000 1750 700 
Wire Wire Line
	1650 700  1650 1000
Wire Wire Line
	1550 1000 1550 700 
Wire Wire Line
	1450 1000 1450 700 
$Comp
L 74xx:74LS21 U4
U 3 1 61F3A262
P 1600 6950
F 0 "U4" H 1830 6996 50  0000 L CNN
F 1 "74HC21" H 1830 6905 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 1600 6950 50  0001 C CNN
F 3 "" H 1600 6950 50  0001 C CNN
	3    1600 6950
	1    0    0    -1  
$EndComp
Entry Wire Line
	1350 600  1450 700 
Entry Wire Line
	1450 600  1550 700 
Entry Wire Line
	1550 600  1650 700 
Entry Wire Line
	1650 600  1750 700 
Entry Wire Line
	2050 600  2150 700 
Entry Wire Line
	2150 600  2250 700 
Entry Wire Line
	2250 600  2350 700 
Entry Wire Line
	2350 600  2450 700 
Entry Wire Line
	2700 600  2800 700 
Entry Wire Line
	2800 600  2900 700 
Entry Wire Line
	2900 600  3000 700 
Entry Wire Line
	3000 600  3100 700 
$Comp
L 74xx:74HC00 U3
U 2 1 62055421
P 3800 5800
F 0 "U3" H 3800 6125 50  0000 C CNN
F 1 "74HC00" H 3800 6034 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 3800 5800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 3800 5800 50  0001 C CNN
	2    3800 5800
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U3
U 3 1 62056F7E
P 2900 6100
F 0 "U3" H 2900 6425 50  0000 C CNN
F 1 "74HC00" H 2900 6334 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 2900 6100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 2900 6100 50  0001 C CNN
	3    2900 6100
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U3
U 5 1 6205A65B
P 850 6950
F 0 "U3" H 1080 6996 50  0000 L CNN
F 1 "74HC00" H 1080 6905 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 850 6950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 850 6950 50  0001 C CNN
	5    850  6950
	1    0    0    -1  
$EndComp
Text Label 4850 2500 0    50   ~ 0
~SLOT2-OE
Text Label 4850 2400 0    50   ~ 0
~SLOT1-OE
Wire Wire Line
	6350 5450 5550 5450
Wire Wire Line
	5550 5550 6350 5550
Wire Wire Line
	6350 2550 5550 2550
Wire Wire Line
	5550 2450 6350 2450
Entry Wire Line
	5450 2450 5550 2550
Entry Wire Line
	5450 2350 5550 2450
Entry Wire Line
	5450 5350 5550 5450
Entry Wire Line
	5450 5450 5550 5550
Entry Wire Line
	5350 2400 5450 2500
Entry Wire Line
	5350 3400 5450 3500
Entry Wire Line
	5350 2500 5450 2600
Entry Wire Line
	5350 4550 5450 4650
Wire Wire Line
	5350 5800 4850 5800
Text Label 4650 750  3    50   ~ 0
A14
Text Label 5050 750  3    50   ~ 0
A15
Text Label 3750 750  3    50   ~ 0
A14
Wire Wire Line
	4250 700  4250 1700
Text Label 4250 750  3    50   ~ 0
A15
Wire Wire Line
	4650 700  4650 1700
Entry Wire Line
	3650 600  3750 700 
Entry Wire Line
	4150 600  4250 700 
Entry Wire Line
	4950 600  5050 700 
Entry Wire Line
	4550 600  4650 700 
$Comp
L 74xx:74HC00 U7
U 1 1 620520B8
P 4150 2000
F 0 "U7" H 4150 2325 50  0000 C CNN
F 1 "74HC00" H 4150 2234 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 4150 2000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 4150 2000 50  0001 C CNN
	1    4150 2000
	0    1    1    0   
$EndComp
$Comp
L 74xx:74HC00 U3
U 4 1 62058435
P 4550 5800
F 0 "U3" H 4550 6125 50  0000 C CNN
F 1 "74HC00" H 4550 6034 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 4550 5800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 4550 5800 50  0001 C CNN
	4    4550 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 1700 4050 1650
Wire Wire Line
	4050 1650 3750 1650
Wire Wire Line
	3750 1650 3750 1600
Wire Wire Line
	5050 1600 5050 1650
Wire Wire Line
	4850 1650 4850 1700
Text Notes 3550 2600 1    50   ~ 0
SLOT 1/2 address decoder
Wire Wire Line
	4750 2300 4750 2400
Wire Wire Line
	4750 2400 5350 2400
Wire Wire Line
	5350 2500 4150 2500
Wire Wire Line
	4150 2500 4150 2300
Text Label 3900 1650 0    50   ~ 0
~A14
Wire Wire Line
	4850 1650 5050 1650
Text Label 4850 1650 0    50   ~ 0
~A15
$Comp
L 74xx:74HC00 U3
U 1 1 62421777
P 1000 1300
F 0 "U3" H 1000 1625 50  0000 C CNN
F 1 "74HC00" H 1000 1534 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 1000 1300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 1000 1300 50  0001 C CNN
	1    1000 1300
	0    1    1    0   
$EndComp
$Comp
L 74xx:74HC00 U7
U 2 1 62424AC5
P 2250 5300
F 0 "U7" H 2250 5625 50  0000 C CNN
F 1 "74HC00" H 2250 5534 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 2250 5300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 2250 5300 50  0001 C CNN
	2    2250 5300
	1    0    0    -1  
$EndComp
Text Label 1100 850  3    50   ~ 0
A14
Text Label 900  850  3    50   ~ 0
A15
Wire Wire Line
	1000 2500 1000 2350
Wire Wire Line
	1100 2500 1100 2350
Wire Wire Line
	1100 2350 1600 2350
Wire Wire Line
	1600 1600 1600 2350
Wire Wire Line
	1200 2500 1200 2400
Wire Wire Line
	1200 2400 2300 2400
Wire Wire Line
	2300 1600 2300 2400
Wire Wire Line
	1300 2500 1300 2450
Wire Wire Line
	1300 2450 2950 2450
Wire Wire Line
	2950 1600 2950 2450
Wire Wire Line
	1100 700  1100 1000
Wire Wire Line
	900  700  900  1000
Entry Wire Line
	800  600  900  700 
Entry Wire Line
	1000 600  1100 700 
$Comp
L 74xx:74HC00 U7
U 3 1 624E37A2
P 2800 3350
F 0 "U7" H 2800 3675 50  0000 C CNN
F 1 "74HC00" H 2800 3584 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 2800 3350 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 2800 3350 50  0001 C CNN
	3    2800 3350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U7
U 4 1 624E5185
P 4750 2000
F 0 "U7" H 4750 2325 50  0000 C CNN
F 1 "74HC00" H 4750 2234 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 4750 2000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 4750 2000 50  0001 C CNN
	4    4750 2000
	0    1    1    0   
$EndComp
Wire Notes Line
	3400 650  3400 2650
$Comp
L 74xx:74AHC04 U6
U 1 1 6252BA7B
P 1000 2050
F 0 "U6" H 1000 2367 50  0000 C CNN
F 1 "74HC04" H 1000 2276 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 1000 2050 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT04.pdf" H 1000 2050 50  0001 C CNN
	1    1000 2050
	0    1    1    0   
$EndComp
$Comp
L 74xx:74AHC04 U6
U 2 1 6252CC93
P 1300 5000
F 0 "U6" H 1300 5317 50  0000 C CNN
F 1 "74HC04" H 1300 5226 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 1300 5000 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT04.pdf" H 1300 5000 50  0001 C CNN
	2    1300 5000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74AHC04 U6
U 3 1 6252E4E4
P 1300 5600
F 0 "U6" H 1300 5917 50  0000 C CNN
F 1 "74HC04" H 1300 5826 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 1300 5600 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT04.pdf" H 1300 5600 50  0001 C CNN
	3    1300 5600
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74AHC04 U6
U 4 1 6252F953
P 2950 5300
F 0 "U6" H 2950 5617 50  0000 C CNN
F 1 "74HC04" H 2950 5526 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 2950 5300 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT04.pdf" H 2950 5300 50  0001 C CNN
	4    2950 5300
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74AHC04 U6
U 5 1 6253119C
P 5050 1300
F 0 "U6" H 5050 1617 50  0000 C CNN
F 1 "74HC04" H 5050 1526 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5050 1300 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT04.pdf" H 5050 1300 50  0001 C CNN
	5    5050 1300
	0    1    1    0   
$EndComp
$Comp
L 74xx:74AHC04 U6
U 6 1 625323BD
P 3750 1300
F 0 "U6" H 3750 1617 50  0000 C CNN
F 1 "74HC04" H 3750 1526 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 3750 1300 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT04.pdf" H 3750 1300 50  0001 C CNN
	6    3750 1300
	0    1    1    0   
$EndComp
Wire Wire Line
	3750 700  3750 1000
Wire Wire Line
	5050 700  5050 1000
$Comp
L 74xx:74LS21 U8
U 1 1 62607AA9
P 4300 3400
F 0 "U8" H 4300 3775 50  0000 C CNN
F 1 "74HC21" H 4300 3684 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 4300 3400 50  0001 C CNN
F 3 "" H 4300 3400 50  0001 C CNN
	1    4300 3400
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS21 U8
U 2 1 62609905
P 4300 4550
F 0 "U8" H 4300 4925 50  0000 C CNN
F 1 "74HC21" H 4300 4834 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 4300 4550 50  0001 C CNN
F 3 "" H 4300 4550 50  0001 C CNN
	2    4300 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 5000 700  5000
Wire Wire Line
	1000 5600 700  5600
Text Label 750  5600 0    50   ~ 0
~MREQ
Text Label 1650 5000 0    50   ~ 0
WR
Text Label 1650 5600 0    50   ~ 0
MREQ
Wire Wire Line
	1900 5600 1900 5400
Wire Wire Line
	1900 5400 1950 5400
Wire Wire Line
	1600 5600 1900 5600
Wire Wire Line
	1600 5000 1900 5000
Wire Wire Line
	1900 5000 1900 5200
Wire Wire Line
	1900 5200 1950 5200
Wire Wire Line
	2550 5300 2650 5300
Wire Wire Line
	3250 5300 3350 5300
Wire Wire Line
	2500 3250 2450 3250
Wire Wire Line
	2450 3250 2450 3350
Wire Wire Line
	2450 3450 2500 3450
Connection ~ 2450 3350
Wire Wire Line
	2450 3350 2450 3450
Text Label 3150 3350 0    50   ~ 0
~A0
Wire Wire Line
	1050 3350 2450 3350
Wire Wire Line
	3100 3350 3400 3350
Text Label 4950 3400 0    50   ~ 0
SLOT1-LD
Text Label 4950 4550 0    50   ~ 0
SLOT2-LD
Entry Wire Line
	600  4400 700  4500
Entry Wire Line
	600  4500 700  4600
Entry Wire Line
	600  4900 700  5000
Entry Wire Line
	600  5500 700  5600
$Comp
L power:GND #PWR08
U 1 1 62A2FB3B
P 10750 4700
F 0 "#PWR08" H 10750 4450 50  0001 C CNN
F 1 "GND" H 10755 4527 50  0000 C CNN
F 2 "" H 10750 4700 50  0001 C CNN
F 3 "" H 10750 4700 50  0001 C CNN
	1    10750 4700
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR03
U 1 1 62A2FB45
P 10750 4100
F 0 "#PWR03" H 10750 3950 50  0001 C CNN
F 1 "VCC" H 10767 4273 50  0000 C CNN
F 2 "" H 10750 4100 50  0001 C CNN
F 3 "" H 10750 4100 50  0001 C CNN
	1    10750 4100
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C5
U 1 1 62A64865
P 8950 5550
F 0 "C5" H 9128 5596 50  0000 L CNN
F 1 "100nF" H 9128 5505 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 8950 5550 50  0001 C CNN
F 3 "~" H 8950 5550 50  0001 C CNN
	1    8950 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 6450 3100 6400
Wire Wire Line
	3900 6450 3900 6400
Wire Wire Line
	4650 6450 4650 6400
Text Label 1300 4400 0    50   ~ 0
A2-A15
$Comp
L 74xx:74LS21 U5
U 3 1 61F3F261
P 2350 6950
F 0 "U5" H 2580 6996 50  0000 L CNN
F 1 "74HC21" H 2580 6905 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 2350 6950 50  0001 C CNN
F 3 "" H 2350 6950 50  0001 C CNN
	3    2350 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 7500 4650 7450
Wire Wire Line
	3900 7500 3900 7450
Wire Wire Line
	3100 7500 3100 7450
$Comp
L pspice:CAP C8
U 1 1 62B13B60
P 10750 5550
F 0 "C8" H 10928 5596 50  0000 L CNN
F 1 "100nF" H 10928 5505 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 10750 5550 50  0001 C CNN
F 3 "~" H 10750 5550 50  0001 C CNN
	1    10750 5550
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C7
U 1 1 62AEB0E8
P 10150 5550
F 0 "C7" H 10328 5596 50  0000 L CNN
F 1 "100nF" H 10328 5505 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 10150 5550 50  0001 C CNN
F 3 "~" H 10150 5550 50  0001 C CNN
	1    10150 5550
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C6
U 1 1 62AC38DF
P 9550 5550
F 0 "C6" H 9728 5596 50  0000 L CNN
F 1 "100nF" H 9728 5505 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 9550 5550 50  0001 C CNN
F 3 "~" H 9550 5550 50  0001 C CNN
	1    9550 5550
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS21 U8
U 3 1 6260B91C
P 4650 6950
F 0 "U8" H 4880 6996 50  0000 L CNN
F 1 "74HC21" H 4880 6905 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 4650 6950 50  0001 C CNN
F 3 "" H 4650 6950 50  0001 C CNN
	3    4650 6950
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74AHC04 U6
U 7 1 625338DB
P 3100 6950
F 0 "U6" H 3330 6996 50  0000 L CNN
F 1 "74HC04" H 3330 6905 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 3100 6950 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT04.pdf" H 3100 6950 50  0001 C CNN
	7    3100 6950
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U7
U 5 1 624E7571
P 3900 6950
F 0 "U7" H 4130 6996 50  0000 L CNN
F 1 "74HC00" H 4130 6905 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 3900 6950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 3900 6950 50  0001 C CNN
	5    3900 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 5800 10750 5850
Wire Wire Line
	10150 5800 10150 5850
Wire Wire Line
	9550 5800 9550 5850
Wire Wire Line
	8950 5800 8950 5850
Wire Wire Line
	10150 4650 10150 4700
Wire Wire Line
	10750 4650 10750 4700
Wire Wire Line
	10750 5250 10750 5300
Wire Wire Line
	10150 5250 10150 5300
Wire Wire Line
	9550 5250 9550 5300
Wire Wire Line
	8950 5250 8950 5300
Wire Wire Line
	10750 4100 10750 4150
Wire Wire Line
	10150 4100 10150 4150
Wire Wire Line
	8950 4700 8950 4650
Wire Wire Line
	9550 4650 9550 4700
Wire Wire Line
	8950 4100 8950 4150
Wire Wire Line
	9550 4100 9550 4150
Connection ~ 8700 2250
$Comp
L power:GND #PWR010
U 1 1 62AC464F
P 9550 5850
F 0 "#PWR010" H 9550 5600 50  0001 C CNN
F 1 "GND" H 9555 5677 50  0000 C CNN
F 2 "" H 9550 5850 50  0001 C CNN
F 3 "" H 9550 5850 50  0001 C CNN
	1    9550 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 62AEBE88
P 10150 5850
F 0 "#PWR011" H 10150 5600 50  0001 C CNN
F 1 "GND" H 10155 5677 50  0000 C CNN
F 2 "" H 10150 5850 50  0001 C CNN
F 3 "" H 10150 5850 50  0001 C CNN
	1    10150 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 62B14930
P 10750 5850
F 0 "#PWR012" H 10750 5600 50  0001 C CNN
F 1 "GND" H 10755 5677 50  0000 C CNN
F 2 "" H 10750 5850 50  0001 C CNN
F 3 "" H 10750 5850 50  0001 C CNN
	1    10750 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 62A655A5
P 8950 5850
F 0 "#PWR09" H 8950 5600 50  0001 C CNN
F 1 "GND" H 8955 5677 50  0000 C CNN
F 2 "" H 8950 5850 50  0001 C CNN
F 3 "" H 8950 5850 50  0001 C CNN
	1    8950 5850
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR07
U 1 1 62B1493A
P 10750 5250
F 0 "#PWR07" H 10750 5100 50  0001 C CNN
F 1 "VCC" H 10767 5423 50  0000 C CNN
F 2 "" H 10750 5250 50  0001 C CNN
F 3 "" H 10750 5250 50  0001 C CNN
	1    10750 5250
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR06
U 1 1 62AEBE92
P 10150 5250
F 0 "#PWR06" H 10150 5100 50  0001 C CNN
F 1 "VCC" H 10167 5423 50  0000 C CNN
F 2 "" H 10150 5250 50  0001 C CNN
F 3 "" H 10150 5250 50  0001 C CNN
	1    10150 5250
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 62AC4659
P 9550 5250
F 0 "#PWR05" H 9550 5100 50  0001 C CNN
F 1 "VCC" H 9567 5423 50  0000 C CNN
F 2 "" H 9550 5250 50  0001 C CNN
F 3 "" H 9550 5250 50  0001 C CNN
	1    9550 5250
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR04
U 1 1 62A655AF
P 8950 5250
F 0 "#PWR04" H 8950 5100 50  0001 C CNN
F 1 "VCC" H 8967 5423 50  0000 C CNN
F 2 "" H 8950 5250 50  0001 C CNN
F 3 "" H 8950 5250 50  0001 C CNN
	1    8950 5250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 63111A03
P 850 7550
F 0 "#PWR0107" H 850 7300 50  0001 C CNN
F 1 "GND" H 855 7377 50  0000 C CNN
F 2 "" H 850 7550 50  0001 C CNN
F 3 "" H 850 7550 50  0001 C CNN
	1    850  7550
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0111
U 1 1 631241F0
P 850 6350
F 0 "#PWR0111" H 850 6200 50  0001 C CNN
F 1 "VCC" H 865 6523 50  0000 C CNN
F 2 "" H 850 6350 50  0001 C CNN
F 3 "" H 850 6350 50  0001 C CNN
	1    850  6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  7450 850  7500
Wire Wire Line
	850  6350 850  6400
Wire Wire Line
	850  6400 1600 6400
Connection ~ 850  6400
Wire Wire Line
	850  6400 850  6450
Connection ~ 3100 6400
Wire Wire Line
	3100 6400 3900 6400
Connection ~ 3900 6400
Wire Wire Line
	3900 6400 4650 6400
Wire Wire Line
	4650 7500 3900 7500
Connection ~ 850  7500
Wire Wire Line
	850  7500 850  7550
Connection ~ 3100 7500
Wire Wire Line
	3100 7500 2350 7500
Connection ~ 3900 7500
Wire Wire Line
	3900 7500 3100 7500
Wire Wire Line
	1600 7450 1600 7500
Connection ~ 1600 7500
Wire Wire Line
	1600 7500 850  7500
Wire Wire Line
	2350 7450 2350 7500
Connection ~ 2350 7500
Wire Wire Line
	2350 7500 1600 7500
Wire Wire Line
	2350 6450 2350 6400
Connection ~ 2350 6400
Wire Wire Line
	2350 6400 3100 6400
Wire Wire Line
	1600 6450 1600 6400
Connection ~ 1600 6400
Wire Wire Line
	1600 6400 2350 6400
Text Label 750  1650 3    50   ~ 0
!(A14·A15)
Wire Wire Line
	2600 6000 2550 6000
Wire Wire Line
	2550 6000 2550 6100
Wire Wire Line
	2550 6200 2600 6200
Connection ~ 2550 6100
Wire Wire Line
	2550 6100 2550 6200
Text Label 750  6100 0    50   ~ 0
~CE
Text Label 3250 6100 0    50   ~ 0
CE
Wire Wire Line
	4100 5800 4200 5800
Wire Wire Line
	4250 5700 4200 5700
Wire Wire Line
	4200 5700 4200 5800
Wire Wire Line
	4200 5900 4250 5900
Connection ~ 4200 5800
Wire Wire Line
	4200 5800 4200 5900
Wire Wire Line
	700  6100 2550 6100
Entry Wire Line
	600  6000 700  6100
Text Label 750  5000 0    50   ~ 0
~WR
Wire Wire Line
	1000 1600 1000 1650
Text Notes 700  650  3    50   ~ 0
Address bus masks
Wire Wire Line
	1000 1650 750  1650
Wire Wire Line
	750  1650 750  2150
Wire Wire Line
	750  2150 700  2150
Connection ~ 1000 1650
Wire Wire Line
	1000 1650 1000 1750
Entry Wire Line
	600  2050 700  2150
Text Label 750  5950 0    50   ~ 0
!(A14·A15)
Wire Wire Line
	700  5950 2350 5950
Wire Wire Line
	2350 5950 2350 5700
Wire Wire Line
	2350 5700 3500 5700
Entry Wire Line
	600  5850 700  5950
Text Notes 1900 2800 0    50   ~ 0
Mapper register logic
Wire Notes Line
	1850 2650 5400 2650
Wire Notes Line
	650  3200 1850 3200
Wire Wire Line
	3200 6100 3400 6100
Wire Wire Line
	3400 6100 3400 5900
Wire Wire Line
	3400 5900 3500 5900
Connection ~ 8700 3550
Wire Bus Line
	8700 2250 8700 3550
$Comp
L Device:R R2
U 1 1 626F93BE
P 7700 2450
F 0 "R2" H 7770 2496 50  0000 L CNN
F 1 "10K" H 7770 2405 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" V 7630 2450 50  0001 C CNN
F 3 "~" H 7700 2450 50  0001 C CNN
	1    7700 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 62750B1A
P 7950 2850
F 0 "R4" H 8020 2896 50  0000 L CNN
F 1 "10K" H 8020 2805 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" V 7880 2850 50  0001 C CNN
F 3 "~" H 7950 2850 50  0001 C CNN
	1    7950 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 3100 7950 3000
$Comp
L Device:R R3
U 1 1 627D53B9
P 8200 2450
F 0 "R3" H 8270 2496 50  0000 L CNN
F 1 "10K" H 8270 2405 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" V 8130 2450 50  0001 C CNN
F 3 "~" H 8200 2450 50  0001 C CNN
	1    8200 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 2600 8200 3100
Wire Wire Line
	7950 1850 7950 2700
Wire Wire Line
	8150 1500 8150 1550
Wire Wire Line
	8150 1200 8150 1150
Connection ~ 8150 1550
Wire Wire Line
	8150 1550 8600 1550
Wire Wire Line
	7350 1550 8150 1550
Wire Wire Line
	8600 1150 8150 1150
Text Label 750  4600 0    50   ~ 0
A1
Text Label 750  4500 0    50   ~ 0
A0
Wire Wire Line
	1050 4500 2050 4500
Wire Wire Line
	700  4500 1050 4500
Connection ~ 1050 4500
Wire Wire Line
	1050 3350 1050 4500
$Comp
L 74xx:74LS21 U5
U 1 1 61F37F40
P 1150 2800
F 0 "U5" H 1150 3175 50  0000 C CNN
F 1 "74HC21" H 1150 3084 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 1150 2800 50  0001 C CNN
F 3 "" H 1150 2800 50  0001 C CNN
	1    1150 2800
	0    -1   1    0   
$EndComp
Wire Wire Line
	4600 4550 5350 4550
Wire Wire Line
	4600 3400 5350 3400
Wire Wire Line
	4000 4400 3900 4400
Wire Wire Line
	3900 4400 3900 3550
Wire Wire Line
	3900 3550 4000 3550
Wire Wire Line
	4000 3450 3800 3450
Wire Wire Line
	3800 4500 4000 4500
Wire Wire Line
	4000 4600 3700 4600
Wire Wire Line
	3700 4600 3700 3350
Wire Wire Line
	3700 3350 4000 3350
Connection ~ 3700 4600
Wire Wire Line
	700  4600 3700 4600
Connection ~ 3900 4400
Wire Wire Line
	1150 3100 1150 4400
Wire Wire Line
	1150 4400 3900 4400
Wire Wire Line
	4000 3250 3400 3250
Wire Wire Line
	3400 3250 3400 3350
Wire Wire Line
	4000 4700 2050 4700
Wire Wire Line
	2050 4700 2050 4500
Text Label 9650 1800 3    50   ~ 0
A15
Wire Notes Line
	650  5800 2050 5800
Wire Notes Line
	3500 5150 5400 5150
Text Notes 3550 5300 0    50   ~ 0
~ROM-CE~ control
Wire Notes Line
	1850 3200 1850 2650
Wire Notes Line
	3500 5550 2050 5550
Wire Notes Line
	2050 5550 2050 5800
Wire Notes Line
	3500 5150 3500 5550
Wire Wire Line
	7700 2600 7700 3100
Wire Wire Line
	8450 3100 8450 3000
$Comp
L Device:R R5
U 1 1 627D56A9
P 8450 2850
F 0 "R5" H 8520 2896 50  0000 L CNN
F 1 "10K" H 8520 2805 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" V 8380 2850 50  0001 C CNN
F 3 "~" H 8450 2850 50  0001 C CNN
	1    8450 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 3100 7950 3100
Connection ~ 7700 3100
Wire Wire Line
	7700 3100 7700 3200
Connection ~ 7950 3100
Wire Wire Line
	7950 3100 8200 3100
Connection ~ 8200 3100
Wire Wire Line
	8200 3100 8450 3100
Wire Wire Line
	8450 2700 8450 1650
Connection ~ 7700 1950
Wire Wire Line
	7700 1950 8600 1950
Connection ~ 7950 1850
Wire Wire Line
	7950 1850 8600 1850
Connection ~ 8200 1750
Wire Wire Line
	8200 1750 8600 1750
Connection ~ 8450 1650
Wire Wire Line
	8450 1650 8600 1650
Wire Wire Line
	7350 1950 7700 1950
Wire Wire Line
	7350 1850 7950 1850
Wire Wire Line
	7350 1750 8200 1750
Wire Wire Line
	7350 1650 8450 1650
Wire Wire Line
	3800 4500 3350 4500
Wire Bus Line
	8700 600  8700 2250
Wire Bus Line
	8700 3550 8700 6350
Wire Bus Line
	600  600  600  7650
Wire Bus Line
	8700 2250 11050 2250
Wire Bus Line
	8700 3550 11050 3550
Wire Bus Line
	600  600  5450 600 
Wire Bus Line
	5450 600  5450 6350
Wire Bus Line
	8700 600  11050 600 
Connection ~ 3800 4500
Wire Wire Line
	3350 4500 3350 5300
Wire Wire Line
	3800 3450 3800 4500
$EndSCHEMATC
