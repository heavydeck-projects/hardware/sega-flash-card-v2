EESchema Schematic File Version 4
LIBS:sega_flash_card_v2-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "SEGA Master System Flash Card"
Date "2019-06-27"
Rev "2.0"
Comp "Heavydeck.net"
Comment1 "See COPYING or https://creativecommons.org/licenses/by-sa/4.0 for the license text"
Comment2 "Released under the Creative Commons Attribution Share-Alike license"
Comment3 "Copyright J.Luis Álvarez 2019"
Comment4 ""
$EndDescr
Wire Wire Line
	5300 2200 5300 1600
Wire Wire Line
	5400 2200 5400 1600
Wire Wire Line
	5500 2200 5500 1600
Wire Wire Line
	5700 2200 5700 1600
Wire Wire Line
	6200 2200 6200 1600
Wire Wire Line
	6300 2200 6300 1600
Wire Wire Line
	6400 2200 6400 1600
Wire Wire Line
	6500 2200 6500 1600
Wire Wire Line
	6600 2200 6600 1600
Wire Wire Line
	6700 2200 6700 1600
Wire Wire Line
	6800 2200 6800 1600
Wire Wire Line
	6900 2200 6900 1600
Wire Wire Line
	7100 2200 7100 1950
Wire Wire Line
	7300 2200 7300 1600
Wire Wire Line
	7400 2200 7400 1600
Wire Wire Line
	7500 2200 7500 1600
Wire Wire Line
	7600 2200 7600 1600
Wire Wire Line
	7700 2200 7700 1600
Wire Wire Line
	7800 2200 7800 1600
Wire Wire Line
	7900 2200 7900 1600
Wire Wire Line
	8000 2200 8000 1600
Wire Wire Line
	8100 2200 8100 1600
Wire Wire Line
	8200 2200 8200 1600
Wire Wire Line
	8300 2200 8300 1600
Wire Wire Line
	8400 2200 8400 1600
Wire Wire Line
	5800 2200 5800 1600
Wire Wire Line
	5900 2200 5900 1600
Wire Wire Line
	6000 2200 6000 1600
Wire Wire Line
	6100 2200 6100 1600
Text Label 5300 2000 1    50   ~ 0
~WR
Text Label 5400 2000 1    50   ~ 0
~MREQ
Text Label 5500 2000 1    50   ~ 0
~RD
Text Label 5600 2150 1    50   ~ 0
~M8-B
Text Label 5700 2000 1    50   ~ 0
A14
Text Label 6200 2000 1    50   ~ 0
~M0-7
Text Label 6300 2000 1    50   ~ 0
A10
Text Label 6400 2000 1    50   ~ 0
~CE
Text Label 6500 2000 1    50   ~ 0
D7
Text Label 6600 2000 1    50   ~ 0
D6
Text Label 6700 2000 1    50   ~ 0
D5
Text Label 6800 2000 1    50   ~ 0
D4
Text Label 6900 2000 1    50   ~ 0
D3
Text Label 7300 2000 1    50   ~ 0
D2
Text Label 7400 2000 1    50   ~ 0
D1
Text Label 7500 2000 1    50   ~ 0
D0
Text Label 7600 2000 1    50   ~ 0
A0
Text Label 7700 2000 1    50   ~ 0
A1
Text Label 7800 2000 1    50   ~ 0
A2
Text Label 7900 2000 1    50   ~ 0
A3
Text Label 8000 2000 1    50   ~ 0
A4
Text Label 8100 2000 1    50   ~ 0
A5
Text Label 8200 2000 1    50   ~ 0
A6
Text Label 8300 2000 1    50   ~ 0
A7
Text Label 8400 2000 1    50   ~ 0
A12
Text Label 5800 2000 1    50   ~ 0
A13
Text Label 5900 2000 1    50   ~ 0
A8
Text Label 6000 2000 1    50   ~ 0
A9
Text Label 6100 2000 1    50   ~ 0
A11
Text Label 8500 2150 1    50   ~ 0
~CONT
$Comp
L Connector_Generic:Conn_01x35 J1
U 1 1 5D156FE3
P 6900 2400
F 0 "J1" V 7025 2396 50  0000 C CNN
F 1 "Conn_01x35" V 7116 2396 50  0000 C CNN
F 2 "SEGA:SEGA_MyCard" H 6900 2400 50  0001 C CNN
F 3 "~" H 6900 2400 50  0001 C CNN
	1    6900 2400
	0    -1   1    0   
$EndComp
$Comp
L power:VCC #PWR0101
U 1 1 5D15EABA
P 5200 1850
F 0 "#PWR0101" H 5200 1700 50  0001 C CNN
F 1 "VCC" H 5217 2023 50  0000 C CNN
F 2 "" H 5200 1850 50  0001 C CNN
F 3 "" H 5200 1850 50  0001 C CNN
	1    5200 1850
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 5D15F0F6
P 8600 2050
F 0 "#PWR0102" H 8600 1900 50  0001 C CNN
F 1 "VCC" V 8617 2223 50  0000 C CNN
F 2 "" H 8600 2050 50  0001 C CNN
F 3 "" H 8600 2050 50  0001 C CNN
	1    8600 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5D15F2A2
P 7100 1850
F 0 "#PWR0103" H 7100 1600 50  0001 C CNN
F 1 "GND" H 7105 1677 50  0000 C CNN
F 2 "" H 7100 1850 50  0001 C CNN
F 3 "" H 7100 1850 50  0001 C CNN
	1    7100 1850
	-1   0    0    1   
$EndComp
Wire Wire Line
	7000 2200 7000 1950
Wire Wire Line
	7000 1950 7100 1950
Connection ~ 7100 1950
Wire Wire Line
	7100 1950 7200 1950
Wire Wire Line
	7200 1950 7200 2200
$Comp
L Memory_Flash:SST39SF040 U1
U 1 1 5D161E49
P 3500 3600
F 0 "U1" H 3500 5081 50  0000 C CNN
F 1 "SST39SF040" H 3500 4990 50  0000 C CNN
F 2 "Housings_DIP:DIP-32_W15.24mm_Socket" H 3500 3900 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/25022B.pdf" H 3500 3900 50  0001 C CNN
	1    3500 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 1850 7100 1950
Wire Wire Line
	5200 1850 5200 2200
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J2
U 1 1 5D16B8CB
P 7050 4400
F 0 "J2" V 7054 3313 50  0000 R CNN
F 1 "Conn_02x20_Odd_Even" V 7145 3313 50  0000 R CNN
F 2 "Pin_Headers:Pin_Header_Angled_2x20_Pitch2.54mm" H 7050 4400 50  0001 C CNN
F 3 "~" H 7050 4400 50  0001 C CNN
	1    7050 4400
	0    1    1    0   
$EndComp
Text Label 1350 4200 0    50   ~ 0
ROM-A18
Text Label 1350 4100 0    50   ~ 0
ROM-A17
Text Label 1350 4000 0    50   ~ 0
ROM-A16
Text Label 1350 3900 0    50   ~ 0
ROM-A15
Text Label 1350 3800 0    50   ~ 0
ROM-A14
Wire Wire Line
	6050 4700 6050 5400
Wire Wire Line
	6150 4700 6150 5400
Wire Wire Line
	6250 4700 6250 5400
Wire Wire Line
	6350 4700 6350 5400
Wire Wire Line
	6450 4700 6450 5400
Text Label 6050 5300 1    50   ~ 0
ROM-A18
Text Label 6150 5300 1    50   ~ 0
ROM-A17
Text Label 6250 5300 1    50   ~ 0
ROM-A16
Text Label 6350 5300 1    50   ~ 0
ROM-A15
Text Label 6450 5300 1    50   ~ 0
ROM-A14
$Comp
L 74xx:74LS32 U2
U 4 1 5D188867
P 1900 4600
F 0 "U2" H 1900 4925 50  0000 C CNN
F 1 "74HC32" H 1900 4834 50  0000 C CNN
F 2 "Housings_DIP:DIP-14_W7.62mm" H 1900 4600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 1900 4600 50  0001 C CNN
	4    1900 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5D1B14AD
P 6050 4000
F 0 "#PWR0104" H 6050 3750 50  0001 C CNN
F 1 "GND" H 6055 3827 50  0000 C CNN
F 2 "" H 6050 4000 50  0001 C CNN
F 3 "" H 6050 4000 50  0001 C CNN
	1    6050 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	6050 4100 6050 4000
Wire Wire Line
	6050 4200 6050 4100
Connection ~ 6050 4100
Wire Wire Line
	6150 4200 6150 4100
Connection ~ 6150 4100
Wire Wire Line
	6150 4100 6050 4100
Wire Wire Line
	6250 4200 6250 4100
Connection ~ 6250 4100
Wire Wire Line
	6250 4100 6150 4100
Wire Wire Line
	6350 4200 6350 4100
Wire Wire Line
	6350 4100 6250 4100
Wire Wire Line
	4700 2600 4100 2600
Wire Wire Line
	4700 2500 4100 2500
Wire Wire Line
	4700 2400 4100 2400
Text Label 4500 2600 2    50   ~ 0
D2
Text Label 4500 2500 2    50   ~ 0
D1
Text Label 4500 2400 2    50   ~ 0
D0
Text Label 2700 2400 2    50   ~ 0
A0
Text Label 2700 2500 2    50   ~ 0
A1
Text Label 2700 2600 2    50   ~ 0
A2
Text Label 2700 2700 2    50   ~ 0
A3
Text Label 2700 2800 2    50   ~ 0
A4
Text Label 2700 2900 2    50   ~ 0
A5
Text Label 2700 3000 2    50   ~ 0
A6
Text Label 2700 3100 2    50   ~ 0
A7
Text Label 2600 3200 0    50   ~ 0
A8
Text Label 2600 3300 0    50   ~ 0
A9
Text Label 2600 3400 0    50   ~ 0
A10
Text Label 2600 3500 0    50   ~ 0
A11
Text Label 2600 3600 0    50   ~ 0
A12
Text Label 2600 3700 0    50   ~ 0
A13
Wire Wire Line
	4100 2700 4700 2700
Wire Wire Line
	4700 2800 4100 2800
Wire Wire Line
	4700 3000 4100 3000
Wire Wire Line
	4700 3100 4100 3100
Wire Wire Line
	4100 2900 4700 2900
Text Label 4400 2700 0    50   ~ 0
D3
Text Label 4400 2800 0    50   ~ 0
D4
Text Label 4400 2900 0    50   ~ 0
D5
Text Label 4400 3000 0    50   ~ 0
D6
Text Label 4400 3100 0    50   ~ 0
D7
Wire Wire Line
	2900 4600 2750 4600
Text Label 2650 5350 1    50   ~ 0
~WR
Text Label 2850 5350 1    50   ~ 0
~RD
$Comp
L power:VCC #PWR0105
U 1 1 5D208232
P 5500 6900
F 0 "#PWR0105" H 5500 6750 50  0001 C CNN
F 1 "VCC" V 5517 7028 50  0000 L CNN
F 2 "" H 5500 6900 50  0001 C CNN
F 3 "" H 5500 6900 50  0001 C CNN
	1    5500 6900
	0    1    1    0   
$EndComp
$Comp
L pspice:CAP C1
U 1 1 5D210654
P 4250 3950
F 0 "C1" H 4428 3996 50  0000 L CNN
F 1 "CAP" H 4428 3905 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 4250 3950 50  0001 C CNN
F 3 "~" H 4250 3950 50  0001 C CNN
	1    4250 3950
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0107
U 1 1 5D23432A
P 4250 3650
F 0 "#PWR0107" H 4250 3500 50  0001 C CNN
F 1 "VCC" H 4267 3823 50  0000 C CNN
F 2 "" H 4250 3650 50  0001 C CNN
F 3 "" H 4250 3650 50  0001 C CNN
	1    4250 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5D234A83
P 4250 4250
F 0 "#PWR0108" H 4250 4000 50  0001 C CNN
F 1 "GND" H 4255 4077 50  0000 C CNN
F 2 "" H 4250 4250 50  0001 C CNN
F 3 "" H 4250 4250 50  0001 C CNN
	1    4250 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 4250 4250 4200
Wire Wire Line
	4250 3700 4250 3650
Text Label 6550 5300 1    50   ~ 0
~WR
Wire Wire Line
	6550 5400 6550 4700
Wire Wire Line
	1600 4500 1250 4500
Text Label 1300 4500 0    50   ~ 0
~CE
Wire Wire Line
	1250 4700 1600 4700
Text Label 1300 4700 0    50   ~ 0
~M0-7
$Comp
L power:VCC #PWR0109
U 1 1 5D266A16
P 7950 4000
F 0 "#PWR0109" H 7950 3850 50  0001 C CNN
F 1 "VCC" H 7967 4173 50  0000 C CNN
F 2 "" H 7950 4000 50  0001 C CNN
F 3 "" H 7950 4000 50  0001 C CNN
	1    7950 4000
	1    0    0    -1  
$EndComp
Text Label 7950 5300 1    50   ~ 0
D4
Wire Wire Line
	7950 4700 7950 5400
Text Label 6550 3600 3    50   ~ 0
~M0-7
Wire Wire Line
	7950 4000 7950 4200
Text Label 7850 5300 1    50   ~ 0
D5
Wire Wire Line
	7850 4700 7850 5400
Text Label 7750 5300 1    50   ~ 0
D6
Wire Wire Line
	7750 4700 7750 5400
Text Label 7650 5300 1    50   ~ 0
D7
Wire Wire Line
	7650 4700 7650 5400
Text Label 7550 5300 1    50   ~ 0
~ROM-CE
Wire Wire Line
	7550 4700 7550 5400
Text Label 7450 5300 1    50   ~ 0
A10
Wire Wire Line
	7450 4700 7450 5400
Text Label 7350 5300 1    50   ~ 0
~RD
Wire Wire Line
	7350 4700 7350 5400
Text Label 7250 5300 1    50   ~ 0
A11
Wire Wire Line
	7250 4700 7250 5400
Text Label 7150 5300 1    50   ~ 0
A9
Wire Wire Line
	7150 4700 7150 5400
Text Label 7050 5300 1    50   ~ 0
A8
Wire Wire Line
	7050 4700 7050 5400
Text Label 6950 5300 1    50   ~ 0
A13
Wire Wire Line
	6950 4700 6950 5400
Text Label 6450 3600 3    50   ~ 0
A14
Wire Wire Line
	6450 3500 6450 4200
Text Label 6650 5300 1    50   ~ 0
~MREQ
Wire Wire Line
	6650 4700 6650 5400
Text Label 7850 3600 3    50   ~ 0
D2
Wire Wire Line
	7850 4200 7850 3500
Text Label 7750 3600 3    50   ~ 0
D1
Wire Wire Line
	7750 4200 7750 3500
Text Label 7650 3600 3    50   ~ 0
D0
Wire Wire Line
	7650 4200 7650 3500
Text Label 7550 3600 3    50   ~ 0
A0
Wire Wire Line
	7550 4200 7550 3500
Text Label 7450 3600 3    50   ~ 0
A1
Wire Wire Line
	7450 4200 7450 3500
Text Label 7350 3600 3    50   ~ 0
A2
Wire Wire Line
	7350 4200 7350 3500
Text Label 7250 3600 3    50   ~ 0
A3
Wire Wire Line
	7250 4200 7250 3500
Text Label 7150 3600 3    50   ~ 0
A4
Wire Wire Line
	7150 4200 7150 3500
Text Label 7050 3600 3    50   ~ 0
A5
Wire Wire Line
	7050 4200 7050 3500
Text Label 6950 3600 3    50   ~ 0
A6
Wire Wire Line
	6950 4200 6950 3500
Text Label 6850 3600 3    50   ~ 0
A7
Wire Wire Line
	6850 4200 6850 3500
Text Label 6750 3600 3    50   ~ 0
A12
Wire Wire Line
	6750 4200 6750 3500
Text Label 6650 3600 3    50   ~ 0
~CE
Wire Wire Line
	6650 4200 6650 3500
Wire Wire Line
	6750 4700 6750 5400
Text Label 6750 5300 1    50   ~ 0
D3
$Comp
L Connector:TestPoint TP1
U 1 1 5D1DC039
P 8500 1900
F 0 "TP1" V 8558 2018 50  0000 L CNN
F 1 "TestPoint" V 8558 1927 50  0001 L CNN
F 2 "TestPoint_Pad_1.5x1.5mm" H 8700 1900 50  0001 C CNN
F 3 "~" H 8700 1900 50  0001 C CNN
	1    8500 1900
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5D1DC972
P 5600 1900
F 0 "TP2" V 5658 2018 50  0000 L CNN
F 1 "TestPoint" V 5658 1927 50  0001 L CNN
F 2 "TestPoint_Pad_1.5x1.5mm" H 5800 1900 50  0001 C CNN
F 3 "~" H 5800 1900 50  0001 C CNN
	1    5600 1900
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0112
U 1 1 5D1F994E
P 6850 4900
F 0 "#PWR0112" H 6850 4750 50  0001 C CNN
F 1 "VCC" H 6868 5073 50  0000 C CNN
F 2 "" H 6850 4900 50  0001 C CNN
F 3 "" H 6850 4900 50  0001 C CNN
	1    6850 4900
	-1   0    0    1   
$EndComp
Wire Wire Line
	6850 4900 6850 4700
Wire Wire Line
	5600 2200 5600 1900
Wire Wire Line
	8500 1900 8500 2200
Text Notes 4900 2700 0    50   ~ 0
SG/SEGA MyCard connector\n(Using Maxim's pin numbering)
NoConn ~ 2900 7100
NoConn ~ 3750 7050
Text Notes 2700 6400 0    50   ~ 0
Remaining connections for OR gate
Connection ~ 3100 7150
Wire Wire Line
	3100 7250 3100 7150
$Comp
L power:GND #PWR0111
U 1 1 5D2C6075
P 3100 7250
F 0 "#PWR0111" H 3100 7000 50  0001 C CNN
F 1 "GND" H 3105 7077 50  0000 C CNN
F 2 "" H 3100 7250 50  0001 C CNN
F 3 "" H 3100 7250 50  0001 C CNN
	1    3100 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 7150 3150 7150
Wire Wire Line
	3100 6950 3100 7150
Wire Wire Line
	3150 6950 3100 6950
Wire Wire Line
	1550 7100 1550 7250
Connection ~ 1550 7100
Wire Wire Line
	1550 6900 1550 7100
$Comp
L power:GND #PWR0110
U 1 1 5D2BEB14
P 1550 7250
F 0 "#PWR0110" H 1550 7000 50  0001 C CNN
F 1 "GND" H 1555 7077 50  0000 C CNN
F 2 "" H 1550 7250 50  0001 C CNN
F 3 "" H 1550 7250 50  0001 C CNN
	1    1550 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 7000 2300 7000
Connection ~ 2250 7000
Wire Wire Line
	2250 7200 2300 7200
Wire Wire Line
	2250 7000 2250 7200
Wire Wire Line
	1550 7100 1600 7100
Wire Wire Line
	1600 6900 1550 6900
Wire Wire Line
	2200 7000 2250 7000
Wire Wire Line
	5450 6900 5500 6900
Connection ~ 5450 6900
Wire Wire Line
	5450 7500 5450 6900
Wire Wire Line
	5150 7500 5450 7500
Wire Wire Line
	4350 6900 4400 6900
Connection ~ 4350 6900
Wire Wire Line
	4350 7500 4650 7500
Wire Wire Line
	4350 6900 4350 7500
Wire Wire Line
	4300 6900 4350 6900
Wire Wire Line
	5400 6900 5450 6900
$Comp
L pspice:CAP C2
U 1 1 5D2114AB
P 4900 7500
F 0 "C2" V 4585 7500 50  0000 C CNN
F 1 "CAP" V 4676 7500 50  0000 C CNN
F 2 "Capacitors_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 4900 7500 50  0001 C CNN
F 3 "~" H 4900 7500 50  0001 C CNN
	1    4900 7500
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5D208E49
P 4300 6900
F 0 "#PWR0106" H 4300 6650 50  0001 C CNN
F 1 "GND" V 4305 6772 50  0000 R CNN
F 2 "" H 4300 6900 50  0001 C CNN
F 3 "" H 4300 6900 50  0001 C CNN
	1    4300 6900
	0    1    1    0   
$EndComp
$Comp
L 74xx:74LS32 U2
U 1 1 5D18BFF4
P 1900 7000
F 0 "U2" H 1900 7325 50  0000 C CNN
F 1 "74HC32" H 1900 7234 50  0000 C CNN
F 2 "Housings_DIP:DIP-14_W7.62mm" H 1900 7000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 1900 7000 50  0001 C CNN
	1    1900 7000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U2
U 5 1 5D189A19
P 4900 6900
F 0 "U2" V 4533 6900 50  0000 C CNN
F 1 "74HC32" V 4624 6900 50  0000 C CNN
F 2 "Housings_DIP:DIP-14_W7.62mm" H 4900 6900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 4900 6900 50  0001 C CNN
	5    4900 6900
	0    1    1    0   
$EndComp
$Comp
L 74xx:74LS32 U2
U 3 1 5D186FEF
P 3450 7050
F 0 "U2" H 3450 7375 50  0000 C CNN
F 1 "74HC32" H 3450 7284 50  0000 C CNN
F 2 "Housings_DIP:DIP-14_W7.62mm" H 3450 7050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 3450 7050 50  0001 C CNN
	3    3450 7050
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U2
U 2 1 5D17D7FE
P 2600 7100
F 0 "U2" H 2600 7425 50  0000 C CNN
F 1 "74HC32" H 2600 7334 50  0000 C CNN
F 2 "Housings_DIP:DIP-14_W7.62mm" H 2600 7100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 2600 7100 50  0001 C CNN
	2    2600 7100
	1    0    0    -1  
$EndComp
Text Notes 1300 5000 0    50   ~ 0
32K Address decoder\n(if no mapper present)
Text Notes 2650 1850 0    50   ~ 0
Card ROM (512KByte)
Entry Wire Line
	6050 5400 6150 5500
Entry Wire Line
	6150 5400 6250 5500
Entry Wire Line
	6250 5400 6350 5500
Entry Wire Line
	6350 5400 6450 5500
Entry Wire Line
	6450 5400 6550 5500
Entry Wire Line
	6550 5400 6650 5500
Entry Wire Line
	6650 5400 6750 5500
Entry Wire Line
	6750 5400 6850 5500
Entry Wire Line
	6950 5400 7050 5500
Entry Wire Line
	7050 5400 7150 5500
Entry Wire Line
	7150 5400 7250 5500
Entry Wire Line
	7250 5400 7350 5500
Entry Wire Line
	7350 5400 7450 5500
Entry Wire Line
	7450 5400 7550 5500
Entry Wire Line
	7550 5400 7650 5500
Entry Wire Line
	7650 5400 7750 5500
Entry Wire Line
	7750 5400 7850 5500
Entry Wire Line
	7850 5400 7950 5500
Entry Wire Line
	7950 5400 8050 5500
Connection ~ 4800 3400
Wire Bus Line
	4800 3400 4800 5500
Entry Wire Line
	6350 3400 6450 3500
Entry Wire Line
	6450 3400 6550 3500
Entry Wire Line
	6550 3400 6650 3500
Entry Wire Line
	6650 3400 6750 3500
Entry Wire Line
	6750 3400 6850 3500
Entry Wire Line
	6850 3400 6950 3500
Entry Wire Line
	6950 3400 7050 3500
Entry Wire Line
	7050 3400 7150 3500
Entry Wire Line
	7150 3400 7250 3500
Entry Wire Line
	7250 3400 7350 3500
Entry Wire Line
	7350 3400 7450 3500
Entry Wire Line
	7450 3400 7550 3500
Entry Wire Line
	7550 3400 7650 3500
Entry Wire Line
	7650 3400 7750 3500
Entry Wire Line
	7750 3400 7850 3500
Wire Wire Line
	6550 3500 6550 4200
Entry Wire Line
	5200 1500 5300 1600
Entry Wire Line
	5300 1500 5400 1600
Entry Wire Line
	5400 1500 5500 1600
Entry Wire Line
	5600 1500 5700 1600
Entry Wire Line
	5700 1500 5800 1600
Entry Wire Line
	5800 1500 5900 1600
Entry Wire Line
	5900 1500 6000 1600
Entry Wire Line
	6000 1500 6100 1600
Entry Wire Line
	6100 1500 6200 1600
Entry Wire Line
	6200 1500 6300 1600
Entry Wire Line
	6300 1500 6400 1600
Entry Wire Line
	6400 1500 6500 1600
Entry Wire Line
	6500 1500 6600 1600
Entry Wire Line
	6600 1500 6700 1600
Entry Wire Line
	6700 1500 6800 1600
Entry Wire Line
	6800 1500 6900 1600
Entry Wire Line
	7200 1500 7300 1600
Entry Wire Line
	7300 1500 7400 1600
Entry Wire Line
	7400 1500 7500 1600
Entry Wire Line
	7500 1500 7600 1600
Entry Wire Line
	7600 1500 7700 1600
Entry Wire Line
	7700 1500 7800 1600
Entry Wire Line
	7800 1500 7900 1600
Entry Wire Line
	7900 1500 8000 1600
Entry Wire Line
	8000 1500 8100 1600
Entry Wire Line
	8100 1500 8200 1600
Entry Wire Line
	8200 1500 8300 1600
Entry Wire Line
	8300 1500 8400 1600
Wire Wire Line
	8600 2050 8600 2200
Text Notes 4900 3900 0    50   ~ 0
Rear pin header
Entry Wire Line
	4700 2400 4800 2500
Entry Wire Line
	4700 2500 4800 2600
Entry Wire Line
	4700 2600 4800 2700
Entry Wire Line
	4700 2700 4800 2800
Entry Wire Line
	4700 2800 4800 2900
Entry Wire Line
	4700 2900 4800 3000
Entry Wire Line
	4700 3000 4800 3100
Entry Wire Line
	4700 3100 4800 3200
Connection ~ 4800 5500
Entry Wire Line
	1150 4400 1250 4500
Entry Wire Line
	1150 4600 1250 4700
Wire Wire Line
	2900 4700 2850 4700
Wire Wire Line
	2850 4700 2850 5400
Wire Wire Line
	2750 4600 2750 5400
Connection ~ 2750 4600
Wire Wire Line
	2750 4600 2200 4600
Wire Wire Line
	2900 4400 2650 4400
Wire Wire Line
	2650 4400 2650 5400
Text Label 2750 5350 1    50   ~ 0
~ROM-CE
Entry Wire Line
	2650 5400 2750 5500
Entry Wire Line
	2750 5400 2850 5500
Entry Wire Line
	2850 5400 2950 5500
Wire Wire Line
	1250 3800 2900 3800
Wire Wire Line
	1250 3900 2900 3900
Wire Wire Line
	1250 4000 2900 4000
Wire Wire Line
	1250 4100 2900 4100
Wire Wire Line
	1250 4200 2900 4200
Wire Wire Line
	1250 3700 2900 3700
Wire Wire Line
	1250 3600 2900 3600
Wire Wire Line
	1250 3500 2900 3500
Wire Wire Line
	1250 3400 2900 3400
Wire Wire Line
	1250 3300 2900 3300
Wire Wire Line
	1250 3200 2900 3200
Wire Wire Line
	1250 3100 2900 3100
Wire Wire Line
	1250 3000 2900 3000
Wire Wire Line
	1250 2900 2900 2900
Wire Wire Line
	1250 2800 2900 2800
Wire Wire Line
	1250 2700 2900 2700
Wire Wire Line
	1250 2600 2900 2600
Wire Wire Line
	1250 2500 2900 2500
Wire Wire Line
	1250 2400 2900 2400
Entry Wire Line
	1150 2300 1250 2400
Entry Wire Line
	1150 2400 1250 2500
Entry Wire Line
	1150 2500 1250 2600
Entry Wire Line
	1150 2600 1250 2700
Entry Wire Line
	1150 2700 1250 2800
Entry Wire Line
	1150 2800 1250 2900
Entry Wire Line
	1150 2900 1250 3000
Entry Wire Line
	1150 3000 1250 3100
Entry Wire Line
	1150 3100 1250 3200
Entry Wire Line
	1150 3200 1250 3300
Entry Wire Line
	1150 3300 1250 3400
Entry Wire Line
	1150 3400 1250 3500
Entry Wire Line
	1150 3500 1250 3600
Entry Wire Line
	1150 3600 1250 3700
Entry Wire Line
	1150 3700 1250 3800
Entry Wire Line
	1150 3800 1250 3900
Entry Wire Line
	1150 3900 1250 4000
Entry Wire Line
	1150 4000 1250 4100
Entry Wire Line
	1150 4100 1250 4200
Text Notes 8750 4950 0    50   ~ 0
The J2 connector is intended to be used by\na mapper circuit, controlling the ROM-A??\nlines, or as a general purpose peripheral\nheader for Master System hardware devel.\n\nIf a mapper is present, the 74HC32 IC at U2\nMUST NOT be populated. If a mapper is missing\nthe U2 must be populated and the following\npins on the J2 connector must be shorted\n(using jumpers, for instance) otherwise many\nROM address pins will be left floating.\n\nPin pairs:\n(31, 32) (33, 34) (35, 36) (37, 38) (39, 40)
Wire Bus Line
	1150 5500 4800 5500
Wire Bus Line
	4800 1500 4800 3400
Wire Bus Line
	1150 1500 1150 5500
Wire Bus Line
	4800 3400 8700 3400
Wire Bus Line
	4800 5500 8700 5500
Wire Bus Line
	4800 1500 8700 1500
Text Notes 8750 3800 0    50   ~ 0
Note:
$EndSCHEMATC
