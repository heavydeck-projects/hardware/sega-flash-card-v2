EESchema Schematic File Version 4
LIBS:mapper-module-f1-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "SEGA Mapper Module"
Date "2019-06-27"
Rev "2.1"
Comp "heavydeck.net"
Comment1 "See COPYING or https://creativecommons.org/licenses/by-sa/4.0 for the license text"
Comment2 "Released under the Creative Commons Attribution Share-Alike license"
Comment3 "Copyright J.Luis Álvarez 2019"
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J1
U 1 1 5D14EA51
P 9800 2600
F 0 "J1" V 9804 1513 50  0000 R CNN
F 1 "Conn_02x20_Odd_Even" V 9895 1513 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x20_P2.54mm_Horizontal" H 9800 2600 50  0001 C CNN
F 3 "~" H 9800 2600 50  0001 C CNN
	1    9800 2600
	0    1    -1   0   
$EndComp
$Comp
L power:VCC #PWR0101
U 1 1 5D14BE5E
P 9600 2200
F 0 "#PWR0101" H 9600 2050 50  0001 C CNN
F 1 "VCC" H 9617 2373 50  0000 C CNN
F 2 "" H 9600 2200 50  0001 C CNN
F 3 "" H 9600 2200 50  0001 C CNN
	1    9600 2200
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 5D14C7F8
P 10700 2900
F 0 "#PWR0102" H 10700 2750 50  0001 C CNN
F 1 "VCC" H 10718 3073 50  0000 C CNN
F 2 "" H 10700 2900 50  0001 C CNN
F 3 "" H 10700 2900 50  0001 C CNN
	1    10700 2900
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5D14CBAA
P 8800 2900
F 0 "#PWR0103" H 8800 2650 50  0001 C CNN
F 1 "GND" H 8805 2727 50  0000 C CNN
F 2 "" H 8800 2900 50  0001 C CNN
F 3 "" H 8800 2900 50  0001 C CNN
	1    8800 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 2900 8800 2850
Wire Wire Line
	8800 2850 8900 2850
Wire Wire Line
	9100 2850 9100 2800
Connection ~ 8800 2850
Wire Wire Line
	8800 2850 8800 2800
Wire Wire Line
	9000 2800 9000 2850
Connection ~ 9000 2850
Wire Wire Line
	9000 2850 9100 2850
Wire Wire Line
	8900 2800 8900 2850
Connection ~ 8900 2850
Wire Wire Line
	8900 2850 9000 2850
Wire Wire Line
	9600 2200 9600 2300
Wire Wire Line
	10700 2900 10700 2800
Text Label 8800 2200 1    50   ~ 0
ROM-A18
Text Label 8900 2200 1    50   ~ 0
ROM-A17
Text Label 9000 2200 1    50   ~ 0
ROM-A16
Text Label 9100 2200 1    50   ~ 0
ROM-A15
Text Label 9200 2200 1    50   ~ 0
ROM-A14
Text Label 9200 2900 3    50   ~ 0
A14
Text Label 9300 2200 1    50   ~ 0
~WR
Text Label 9400 2200 1    50   ~ 0
~MREQ
Text Label 9500 2200 1    50   ~ 0
D3
Text Label 9700 2200 1    50   ~ 0
A13
Text Label 9800 2200 1    50   ~ 0
A8
Text Label 9900 2200 1    50   ~ 0
A9
Text Label 10000 2200 1    50   ~ 0
A11
Text Label 10100 2200 1    50   ~ 0
~RD
Text Label 10200 2200 1    50   ~ 0
A10
Text Label 10300 2200 1    50   ~ 0
~ROM-CE
Text Label 10700 2200 1    50   ~ 0
D4
Text Label 9300 2900 3    50   ~ 0
~M0-7
Text Label 9400 2900 3    50   ~ 0
~CE
Text Label 9500 2900 3    50   ~ 0
A12
Text Label 9600 2900 3    50   ~ 0
A7
Text Label 9700 2900 3    50   ~ 0
A6
Text Label 9800 2900 3    50   ~ 0
A5
Text Label 9900 2900 3    50   ~ 0
A4
Text Label 10000 2900 3    50   ~ 0
A3
Text Label 10100 2900 3    50   ~ 0
A2
Text Label 10200 2900 3    50   ~ 0
A1
Text Label 10300 2900 3    50   ~ 0
A0
Text Label 10400 2900 3    50   ~ 0
D0
Text Label 10500 2900 3    50   ~ 0
D1
Text Label 10600 2900 3    50   ~ 0
D2
Wire Wire Line
	9200 1800 9200 2300
Wire Wire Line
	9300 1800 9300 2300
Wire Wire Line
	9400 1800 9400 2300
Wire Wire Line
	9500 1800 9500 2300
Wire Wire Line
	9700 1800 9700 2300
Wire Wire Line
	9800 1800 9800 2300
Wire Wire Line
	9900 2300 9900 1800
Wire Wire Line
	10000 1800 10000 2300
Wire Wire Line
	10100 2300 10100 1800
Wire Wire Line
	10200 1800 10200 2300
Wire Wire Line
	10300 2300 10300 1800
Wire Wire Line
	10700 2300 10700 1800
Wire Wire Line
	9200 2800 9200 3250
Wire Wire Line
	9300 3250 9300 2800
Wire Wire Line
	9400 2800 9400 3250
Wire Wire Line
	9500 3250 9500 2800
Wire Wire Line
	9600 2800 9600 3250
Wire Wire Line
	9700 3250 9700 2800
Wire Wire Line
	9800 2800 9800 3250
Wire Wire Line
	9900 3250 9900 2800
Wire Wire Line
	10000 2800 10000 3250
Wire Wire Line
	10100 3250 10100 2800
Wire Wire Line
	10200 2800 10200 3250
Wire Wire Line
	10300 3250 10300 2800
Wire Wire Line
	10400 2800 10400 3250
Wire Wire Line
	10500 3250 10500 2800
Wire Wire Line
	10600 2800 10600 3250
$Comp
L Device:R_Network04 RN1
U 1 1 5D1938E2
P 8250 1550
F 0 "RN1" V 8575 1550 50  0000 C CNN
F 1 "10K" V 8484 1550 50  0000 C CNN
F 2 "Resistor_THT:R_Array_SIP5" V 8525 1550 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 8250 1550 50  0001 C CNN
	1    8250 1550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8450 1750 9100 1750
Wire Wire Line
	9100 1750 9100 2300
Wire Wire Line
	8450 1650 9000 1650
Wire Wire Line
	9000 1650 9000 2300
Wire Wire Line
	8450 1550 8900 1550
Wire Wire Line
	8900 1550 8900 2300
Wire Wire Line
	8450 1450 8800 1450
Wire Wire Line
	8800 1450 8800 2300
$Comp
L power:GND #PWR0120
U 1 1 5D1B3B76
P 8000 1800
F 0 "#PWR0120" H 8000 1550 50  0001 C CNN
F 1 "GND" H 8005 1627 50  0000 C CNN
F 2 "" H 8000 1800 50  0001 C CNN
F 3 "" H 8000 1800 50  0001 C CNN
	1    8000 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 1800 8000 1750
Wire Wire Line
	8000 1750 8050 1750
Wire Wire Line
	8800 1450 8800 1150
Connection ~ 8800 1450
Wire Wire Line
	8900 1150 8900 1550
Connection ~ 8900 1550
Wire Wire Line
	9000 1650 9000 1150
Connection ~ 9000 1650
Wire Wire Line
	9100 1150 9100 1750
Connection ~ 9100 1750
NoConn ~ 10400 2300
NoConn ~ 10500 2300
NoConn ~ 10600 2300
$Comp
L Device:R R1
U 1 1 5D157921
P 9200 1650
F 0 "R1" H 9270 1696 50  0000 L CNN
F 1 "10K" H 9270 1605 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 9130 1650 50  0001 C CNN
F 3 "~" H 9200 1650 50  0001 C CNN
	1    9200 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 1500 9200 1150
Text Label 9200 1250 3    50   ~ 0
A14
$Comp
L Memory_Flash:SST39SF040 U1
U 1 1 5D1DF0FB
P 2550 4150
F 0 "U1" H 2550 5631 50  0000 C CNN
F 1 "SST39SF040" H 2550 5540 50  0000 C CNN
F 2 "Housings_LCC:PLCC-32" H 2550 4450 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/25022B.pdf" H 2550 4450 50  0001 C CNN
	1    2550 4150
	1    0    0    -1  
$EndComp
Text Label 1550 2950 0    50   ~ 0
A0
Text Label 1550 3050 0    50   ~ 0
A1
Text Label 1550 3150 0    50   ~ 0
A2
Text Label 1550 3250 0    50   ~ 0
A3
Text Label 1550 3350 0    50   ~ 0
A4
Text Label 1550 3450 0    50   ~ 0
A5
Text Label 1550 3550 0    50   ~ 0
A6
Text Label 1550 3650 0    50   ~ 0
A7
Text Label 1550 3750 0    50   ~ 0
A8
Text Label 1550 3850 0    50   ~ 0
A9
Text Label 1550 3950 0    50   ~ 0
A10
Text Label 1550 4050 0    50   ~ 0
A11
Text Label 1550 4150 0    50   ~ 0
A12
Text Label 1550 4250 0    50   ~ 0
A13
Text Label 1550 4350 0    50   ~ 0
A14
Text Label 1550 4450 0    50   ~ 0
A15
Text Label 3250 3650 0    50   ~ 0
SLOT2-LD
Text Label 3250 3550 0    50   ~ 0
SLOT1-LD
Wire Wire Line
	3150 3650 3650 3650
Wire Wire Line
	3150 3550 3700 3550
Wire Wire Line
	3150 3450 3750 3450
Text Label 3250 3350 0    50   ~ 0
~SLOT1-OE
Text Label 3250 3250 0    50   ~ 0
~ROM-CE
Text Label 3250 3450 0    50   ~ 0
~SLOT2-OE
Text Label 1550 4650 0    50   ~ 0
~CE
Text Label 1550 4550 0    50   ~ 0
~WR
Text Label 1550 5250 0    50   ~ 0
~MREQ
Text Label 1550 4750 0    50   ~ 0
~RD
$Comp
L Device:R R2
U 1 1 5D232328
P 3500 3950
F 0 "R2" V 3293 3950 50  0000 C CNN
F 1 "R" V 3384 3950 50  0000 C CNN
F 2 "" V 3430 3950 50  0001 C CNN
F 3 "~" H 3500 3950 50  0001 C CNN
	1    3500 3950
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5D23358F
P 3550 4250
F 0 "R3" V 3343 4250 50  0000 C CNN
F 1 "R" V 3434 4250 50  0000 C CNN
F 2 "" V 3480 4250 50  0001 C CNN
F 3 "~" H 3550 4250 50  0001 C CNN
	1    3550 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	3650 3950 3650 3650
Connection ~ 3650 3650
Wire Wire Line
	3700 4250 3700 3550
Connection ~ 3700 3550
$Comp
L power:GND #PWR0104
U 1 1 5D23DE9B
P 3250 4300
F 0 "#PWR0104" H 3250 4050 50  0001 C CNN
F 1 "GND" H 3255 4127 50  0000 C CNN
F 2 "" H 3250 4300 50  0001 C CNN
F 3 "" H 3250 4300 50  0001 C CNN
	1    3250 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 4250 3250 4250
Wire Wire Line
	3250 4250 3250 3950
Wire Wire Line
	3250 3950 3350 3950
$Comp
L Device:R R4
U 1 1 5D2422AC
P 3600 4550
F 0 "R4" V 3393 4550 50  0000 C CNN
F 1 "R" V 3484 4550 50  0000 C CNN
F 2 "" V 3530 4550 50  0001 C CNN
F 3 "~" H 3600 4550 50  0001 C CNN
	1    3600 4550
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5D242704
P 3650 4850
F 0 "R5" V 3443 4850 50  0000 C CNN
F 1 "R" V 3534 4850 50  0000 C CNN
F 2 "" V 3580 4850 50  0001 C CNN
F 3 "~" H 3650 4850 50  0001 C CNN
	1    3650 4850
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0105
U 1 1 5D246EA0
P 3250 5200
F 0 "#PWR0105" H 3250 5050 50  0001 C CNN
F 1 "VCC" H 3268 5373 50  0000 C CNN
F 2 "" H 3250 5200 50  0001 C CNN
F 3 "" H 3250 5200 50  0001 C CNN
	1    3250 5200
	-1   0    0    1   
$EndComp
Wire Wire Line
	3250 4850 3250 4550
Wire Wire Line
	3250 4550 3450 4550
Wire Wire Line
	3500 4850 3250 4850
Wire Wire Line
	3250 4300 3250 4250
Connection ~ 3250 4250
Connection ~ 3250 4850
$Comp
L Device:R R6
U 1 1 5D25938F
P 3700 5150
F 0 "R6" V 3493 5150 50  0000 C CNN
F 1 "R" V 3584 5150 50  0000 C CNN
F 2 "" V 3630 5150 50  0001 C CNN
F 3 "~" H 3700 5150 50  0001 C CNN
	1    3700 5150
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 5150 3550 5150
Wire Wire Line
	3250 4850 3250 5150
Connection ~ 3250 5150
Wire Wire Line
	3250 5150 3250 5200
Wire Wire Line
	3150 3350 3800 3350
Wire Wire Line
	3150 3250 3850 3250
Wire Wire Line
	3750 4550 3750 3450
Connection ~ 3750 3450
Wire Wire Line
	3800 4850 3800 3350
Connection ~ 3800 3350
Wire Wire Line
	3850 5150 3850 3250
Connection ~ 3850 3250
$Comp
L power:VCC #PWR0106
U 1 1 5D28A145
P 1500 4950
F 0 "#PWR0106" H 1500 4800 50  0001 C CNN
F 1 "VCC" V 1518 5077 50  0000 L CNN
F 2 "" H 1500 4950 50  0001 C CNN
F 3 "" H 1500 4950 50  0001 C CNN
	1    1500 4950
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5D28D8F3
P 1500 5150
F 0 "#PWR0107" H 1500 4900 50  0001 C CNN
F 1 "GND" V 1505 5022 50  0000 R CNN
F 2 "" H 1500 5150 50  0001 C CNN
F 3 "" H 1500 5150 50  0001 C CNN
	1    1500 5150
	0    1    1    0   
$EndComp
Wire Wire Line
	1500 5150 1950 5150
Wire Wire Line
	1950 4950 1500 4950
Wire Wire Line
	1150 5250 1950 5250
Wire Wire Line
	1150 4750 1950 4750
Wire Wire Line
	1150 4650 1950 4650
Wire Wire Line
	1150 4550 1950 4550
Wire Wire Line
	1150 4450 1950 4450
Wire Wire Line
	1150 4350 1950 4350
Wire Wire Line
	1150 4250 1950 4250
Wire Wire Line
	1150 4150 1950 4150
Wire Wire Line
	1150 3950 1950 3950
Wire Wire Line
	1150 4050 1950 4050
Wire Wire Line
	1150 3850 1950 3850
Wire Wire Line
	1150 3750 1950 3750
Wire Wire Line
	1150 3650 1950 3650
Wire Wire Line
	1150 3450 1950 3450
Wire Wire Line
	1150 3550 1950 3550
Wire Wire Line
	1150 3350 1950 3350
Wire Wire Line
	1150 3250 1950 3250
Wire Wire Line
	1150 3150 1950 3150
Wire Wire Line
	1150 3050 1950 3050
Wire Wire Line
	1150 2950 1950 2950
NoConn ~ 3150 3150
NoConn ~ 3150 3050
NoConn ~ 3150 2950
Wire Wire Line
	3650 3650 4050 3650
Wire Wire Line
	3700 3550 4050 3550
Wire Wire Line
	3800 3350 4050 3350
Wire Wire Line
	3750 3450 4050 3450
Wire Wire Line
	3850 3250 4050 3250
$Comp
L power:GND #PWR0108
U 1 1 5D307FE6
P 2550 5450
F 0 "#PWR0108" H 2550 5200 50  0001 C CNN
F 1 "GND" H 2555 5277 50  0000 C CNN
F 2 "" H 2550 5450 50  0001 C CNN
F 3 "" H 2550 5450 50  0001 C CNN
	1    2550 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 5450 2550 5350
$Comp
L power:VCC #PWR0109
U 1 1 5D30B945
P 2550 2550
F 0 "#PWR0109" H 2550 2400 50  0001 C CNN
F 1 "VCC" H 2567 2723 50  0000 C CNN
F 2 "" H 2550 2550 50  0001 C CNN
F 3 "" H 2550 2550 50  0001 C CNN
	1    2550 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 2550 2550 2850
$Comp
L 74xx:74LS573 U2
U 1 1 5D30FEDB
P 6050 2150
F 0 "U2" H 6050 3131 50  0000 C CNN
F 1 "74LS573" H 6050 3040 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 6050 2150 50  0001 C CNN
F 3 "74xx/74hc573.pdf" H 6050 2150 50  0001 C CNN
	1    6050 2150
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS573 U3
U 1 1 5D312206
P 6050 4700
F 0 "U3" H 6050 5681 50  0000 C CNN
F 1 "74LS573" H 6050 5590 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 6050 4700 50  0001 C CNN
F 3 "74xx/74hc573.pdf" H 6050 4700 50  0001 C CNN
	1    6050 4700
	1    0    0    -1  
$EndComp
Text Label 5250 1650 0    50   ~ 0
D0
Text Label 5250 1750 0    50   ~ 0
D1
Text Label 5250 1850 0    50   ~ 0
D2
Text Label 5250 1950 0    50   ~ 0
D3
Text Label 5250 2050 0    50   ~ 0
D4
Text Label 5250 2150 0    50   ~ 0
D5
Text Label 5250 2250 0    50   ~ 0
D6
Text Label 5250 2350 0    50   ~ 0
D7
Text Label 6600 1650 0    50   ~ 0
ROM-A14
Text Label 6600 1750 0    50   ~ 0
ROM-A15
Text Label 6600 1850 0    50   ~ 0
ROM-A16
Text Label 6600 1950 0    50   ~ 0
ROM-A17
Text Label 6600 2050 0    50   ~ 0
ROM-A18
Text Label 6600 2150 0    50   ~ 0
ROM-A19
Text Label 6600 2250 0    50   ~ 0
ROM-A20
Text Label 6600 2350 0    50   ~ 0
ROM-A21
Wire Wire Line
	6550 2350 7000 2350
Wire Wire Line
	6550 2250 7000 2250
Wire Wire Line
	6550 2150 7000 2150
Wire Wire Line
	6550 2050 7000 2050
Wire Wire Line
	6550 1950 7000 1950
Wire Wire Line
	6550 1850 7000 1850
Wire Wire Line
	6550 1750 7000 1750
Wire Wire Line
	6550 1650 7000 1650
Text Label 6600 4200 0    50   ~ 0
ROM-A14
Text Label 6600 4300 0    50   ~ 0
ROM-A15
Text Label 6600 4400 0    50   ~ 0
ROM-A16
Text Label 6600 4500 0    50   ~ 0
ROM-A17
Text Label 6600 4600 0    50   ~ 0
ROM-A18
Text Label 6600 4700 0    50   ~ 0
ROM-A19
Text Label 6600 4800 0    50   ~ 0
ROM-A20
Text Label 6600 4900 0    50   ~ 0
ROM-A21
Wire Wire Line
	6550 4900 7000 4900
Wire Wire Line
	6550 4800 7000 4800
Wire Wire Line
	6550 4700 7000 4700
Wire Wire Line
	6550 4600 7000 4600
Wire Wire Line
	6550 4500 7000 4500
Wire Wire Line
	6550 4400 7000 4400
Wire Wire Line
	6550 4300 7000 4300
Wire Wire Line
	6550 4200 7000 4200
Text Label 5250 4200 0    50   ~ 0
D0
Text Label 5250 4300 0    50   ~ 0
D1
Text Label 5250 4400 0    50   ~ 0
D2
Text Label 5250 4500 0    50   ~ 0
D3
Text Label 5250 4600 0    50   ~ 0
D4
Text Label 5250 4700 0    50   ~ 0
D5
Text Label 5250 4800 0    50   ~ 0
D6
Text Label 5250 4900 0    50   ~ 0
D7
Text Label 5100 5100 0    50   ~ 0
SLOT2-LD
Text Label 5100 2550 0    50   ~ 0
SLOT1-LD
Text Label 5100 2650 0    50   ~ 0
~SLOT1-OE
Text Label 5100 5200 0    50   ~ 0
~SLOT2-OE
Wire Wire Line
	5550 2650 5000 2650
Wire Wire Line
	5000 2550 5550 2550
Wire Wire Line
	5000 2250 5550 2250
Wire Wire Line
	5000 2350 5550 2350
Wire Wire Line
	5000 2150 5550 2150
Wire Wire Line
	5000 2050 5550 2050
Wire Wire Line
	5000 1950 5550 1950
Wire Wire Line
	5000 1850 5550 1850
Wire Wire Line
	5000 1750 5550 1750
Wire Wire Line
	5000 1650 5550 1650
Wire Wire Line
	5000 4800 5550 4800
Wire Wire Line
	5000 4900 5550 4900
Wire Wire Line
	5000 4700 5550 4700
Wire Wire Line
	5000 4600 5550 4600
Wire Wire Line
	5000 4500 5550 4500
Wire Wire Line
	5000 4400 5550 4400
Wire Wire Line
	5000 4300 5550 4300
Wire Wire Line
	5000 4200 5550 4200
Wire Wire Line
	5550 5200 5000 5200
Wire Wire Line
	5000 5100 5550 5100
$Comp
L power:VCC #PWR?
U 1 1 5D2E5F80
P 6050 3650
F 0 "#PWR?" H 6050 3500 50  0001 C CNN
F 1 "VCC" H 6067 3823 50  0000 C CNN
F 2 "" H 6050 3650 50  0001 C CNN
F 3 "" H 6050 3650 50  0001 C CNN
	1    6050 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3650 6050 3900
$Comp
L power:VCC #PWR?
U 1 1 5D2ECBF3
P 6050 1100
F 0 "#PWR?" H 6050 950 50  0001 C CNN
F 1 "VCC" H 6067 1273 50  0000 C CNN
F 2 "" H 6050 1100 50  0001 C CNN
F 3 "" H 6050 1100 50  0001 C CNN
	1    6050 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 1100 6050 1350
$Comp
L power:GND #PWR?
U 1 1 5D2F3A63
P 6050 3000
F 0 "#PWR?" H 6050 2750 50  0001 C CNN
F 1 "GND" H 6055 2827 50  0000 C CNN
F 2 "" H 6050 3000 50  0001 C CNN
F 3 "" H 6050 3000 50  0001 C CNN
	1    6050 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3000 6050 2950
$Comp
L power:GND #PWR?
U 1 1 5D2FAEF2
P 6050 5550
F 0 "#PWR?" H 6050 5300 50  0001 C CNN
F 1 "GND" H 6055 5377 50  0000 C CNN
F 2 "" H 6050 5550 50  0001 C CNN
F 3 "" H 6050 5550 50  0001 C CNN
	1    6050 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 5550 6050 5500
$EndSCHEMATC
